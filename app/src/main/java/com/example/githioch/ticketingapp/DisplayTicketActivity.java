package com.example.githioch.ticketingapp;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * Created by Githioch on 6/2/2015.
 */
public class DisplayTicketActivity extends ActionBarActivity{

    private ImageView imageView;
    private String qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        qr = getIntent().getStringExtra("qr");

        setContentView(R.layout.fragment_display_ticket);
        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setTitle("Ticket");
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setDisplayShowTitleEnabled(true);
        menu.setBackgroundDrawable(getResources().getDrawable(R.color.background_material_dark));

        imageView = (ImageView) findViewById(R.id.display_ticket);

        try {
            generateQrCode();
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    // method that generates qr code
    private void generateQrCode() throws WriterException {


        com.google.zxing.Writer writer = new QRCodeWriter();
        String finaldata = Uri.encode(qr, "utf-8");

        int QR_CODE_WIDTH = 500;
        int QR_CODE_HEIGHT = 500;
        BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE, QR_CODE_WIDTH, QR_CODE_HEIGHT);
        Bitmap ImageBitmap = Bitmap.createBitmap(QR_CODE_WIDTH, QR_CODE_HEIGHT, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < QR_CODE_WIDTH; i++) {//width
            for (int j = 0; j < QR_CODE_HEIGHT; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
            }
        }

        imageView.setImageBitmap(ImageBitmap);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //slide from left to right
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
