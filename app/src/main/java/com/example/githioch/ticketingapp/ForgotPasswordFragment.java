package com.example.githioch.ticketingapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.SQLiteHandler;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

/**
 * Created by Githioch on 1/20/2015.
 * For the user to register an account
 */
public class ForgotPasswordFragment extends Fragment {

    private static String TAG = ForgotPasswordFragment.class.getSimpleName();

    String jsonresult = null;
    private TextView displayview = null;
    private String username;
    private String emailaddress;
    private String password1;
    private String password2;
    private EditText emailaddressview;

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private String activity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reset_password, container, false);

        Button sendEmail = (Button) rootView.findViewById(R.id.send_email);
        Button linklogin = (Button) rootView.findViewById(R.id.btnLinkToLoginScreen);
        final EditText emial = (EditText) rootView.findViewById(R.id.email);

        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(true);

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectionDetector connectionDetector = new ConnectionDetector(getActivity()
                        .getApplicationContext());
                Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

                if (isInternetPresent) {

                        String emailstring = emial.getText()
                                .toString().trim();

                        boolean validemailaddress = validateEmialAddress(emailstring);

                        if (validemailaddress) {
                            saveEmailAddressAndSendResetLink(emailstring);
                        }
                } else {
                    hideDialog();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Please connect to the Internet and try again.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });


        // Link to Login Screen
        linklogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(),
                        SignInActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });

        return rootView;
    }

    private void saveEmailAddressAndSendResetLink(String emailstring) {

        pDialog.setMessage("Sending email ...");
        showDialog();

        ParseUser.requestPasswordResetInBackground(emailstring, new RequestPasswordResetCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    Log.d("emial", "successfully sent");
                    hideDialog();
                    Toast.makeText(getActivity().getApplicationContext(), "Email sent successfully", Toast.LENGTH_LONG)
                            .show();
                    Intent i = new Intent(getActivity().getApplicationContext(),
                            SignInActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else {
                    // Something went wrong. Look at the ParseException to see what's up.
                    Log.d("error", e.getMessage());
                }
            }
        });

    }

    private boolean validateEmialAddress(String s) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        // get values from repesctive fields
        if (!s.equals("")) {
            emailaddress = s;

            if (!emailaddress.matches(emailPattern)) {
                Toast.makeText(getActivity().getApplicationContext(), "Invalid email address."
                        , Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        } else {

            Toast.makeText(getActivity().getApplicationContext(), "Please fill in your email address."
                    , Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    private void hideDialog() {

        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();

    }

}




