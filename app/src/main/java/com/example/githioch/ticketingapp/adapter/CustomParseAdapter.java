package com.example.githioch.ticketingapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.model.Movie;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.List;

/**
 * Created by Githioch on 3/30/2015.
 */
public class CustomParseAdapter extends BaseAdapter{

    ImageLoader imageLoader;
    private final Context context;
    private List<Movie> movieItems;

    public CustomParseAdapter(Context context, List<Movie> movies){
        this.movieItems = movies;
        this.context = context;
        imageLoader = ImageLoader.getInstance();

    }

    @Override
    public int getCount() {
        return movieItems.size();
    }

    @Override
    public Object getItem(int position) {
        return movieItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = View.inflate(context, R.layout.list_row, null);
        }
        // Add and download the image
        ParseImageView parseImageView = (ParseImageView) convertView.findViewById(R.id.thumbnail);
        final ParseFile imageFile = movieItems.get(position).getMovieImage();
        if (imageFile != null) {
            parseImageView.setParseFile(imageFile);
            parseImageView.loadInBackground();
        }
        // Add the title view
        TextView title = (TextView) convertView.findViewById(R.id.title);

        //remove underscores from movie name
        String movie_name = movieItems.get(position).getMovieName();
        movie_name = movie_name.replaceAll("_"," ");
        title.setText(movie_name);

        TextView venue = (TextView) convertView.findViewById(R.id.venue);
        venue.setText(movieItems.get(position).getVenue());

        return convertView;
    }
}
