package com.example.githioch.ticketingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.adapter.CustomParseAdapter;
import com.example.githioch.ticketingapp.model.Movie;
import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.SQLiteHandler;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 2/21/2015.
 * For managing the event items shown in the main view during app launch
 */
//TODO error from renderscript
    //TODO movies reordering when returning
public class MovieListFragment extends ListFragment {

    // Log tag
    private static final String TAG = MovieListFragment.class.getSimpleName();

    List<ParseObject> eventlistorig;
    ArrayAdapter<String> adapter = null;
    private ParseQueryAdapter parseQueryAdapter;
    private CustomParseAdapter customParseAdapter;
    private OnMovieSelectedListener mListener;
    private ProgressDialog pDialog;
    private List<Movie> movieList = new ArrayList<>();
    private SQLiteHandler db;

    private List<Movie> eventlist = new ArrayList<>();
    private ProgressWheel wheel;
    private ListView listView;
    private Button retry;
    private TextView no_internet;
    private LinearLayout mainLayout;
    private int top = 0;
    private int index = 0;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnMovieSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnEventSelectedListener");
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        movieList = new ArrayList<>();

        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        if(isInternetPresent) {
            Log.d("internet", "present");

            queryParseAndLoadAdapter();

        }
        else{
            mainLayout.setVisibility(View.GONE);
            wheel.setVisibility(View.GONE);
            retry.setVisibility(View.VISIBLE);
            no_internet.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);
        mainLayout = (LinearLayout)rootView.findViewById(R.id.movie_list_main);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        wheel  = (ProgressWheel) rootView.findViewById(R.id.progress_wheel);
        no_internet = (TextView) rootView.findViewById(R.id.no_internet);
        retry = (Button) rootView.findViewById(R.id.retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
                boolean isInternetPresent = connectionDetector.isConnectingToInternet();
                retryFunction(isInternetPresent);
            }
        });


        return rootView;
    }

    private void retryFunction(boolean isInternetPresent) {

        if(isInternetPresent) {

            no_internet.setVisibility(View.GONE);
            retry.setVisibility(View.GONE);
            wheel.setVisibility(View.VISIBLE);
            wheel.spin();

            queryParseAndLoadAdapter();
        }
        else{
            wheel.setVisibility(View.GONE);
            retry.setVisibility(View.VISIBLE);
            no_internet.setVisibility(View.VISIBLE);

        }
    }

    private void queryParseAndLoadAdapter() {

        ParseQuery<ParseObject> objectParseQuery = new ParseQuery<ParseObject>("movies");
        objectParseQuery.fromLocalDatastore();
        objectParseQuery.orderByDescending("movieName");
        objectParseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.size() == 0){
                        getMoviesFromParseAndStoreLocally();
                    }else{
                        loadMoviesFromLocalDatastore(list);
                    }
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "Movies cannot be loaded " +
                            e.getMessage() + ". Please try again later", Toast.LENGTH_SHORT).show();
                }

            }
        });

        hidePDialog();
    }

    private void loadMoviesFromLocalDatastore(List<ParseObject> list) {
        Log.d("List", "not null");
        Log.d("list size", String.valueOf(list.size()));
        for (int i = 0; i < list.size(); i++) {
            Movie movie  = new Movie(list.get(i).getString("movieName"),
                    list.get(i).getString("venue"),
                    list.get(i).getString("movieDescription"),
                    list.get(i).getParseFile("movieImage"));

            movieList.add(movie);
        }
        customParseAdapter = new CustomParseAdapter(getActivity(), movieList);
        listView.setAdapter(customParseAdapter);
        listView.setSelectionFromTop(index, top);
        wheel.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
    }

    private void getMoviesFromParseAndStoreLocally() {
        Log.d("list from store", "null");
        ParseQuery<ParseObject> objectParseQuery = new ParseQuery<ParseObject>("movies");
        objectParseQuery.orderByDescending("movieName");
        objectParseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {

                if (e == null) {
                    // USING PARSE FOR GETTING INFO
                    for (int i = 0; i < list.size(); i++) {
                        Movie movie  = new Movie(list.get(i).getString("movieName"),
                                list.get(i).getString("venue"),
                                list.get(i).getString("movieDescription"),
                                list.get(i).getParseFile("movieImage"));

                        movieList.add(movie);
                    }
                    ParseObject.pinAllInBackground(list);
                    customParseAdapter = new CustomParseAdapter(getActivity(), movieList);
                    listView.setAdapter(customParseAdapter);
                    wheel.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                } else { // if there's an error message
                    Toast.makeText(getActivity().getApplicationContext(), "Movies cannot be loaded " +
                            e.getMessage() + ". Please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Movie movie = (Movie) customParseAdapter.getItem(position);
        String name = movie.getMovieName();
        mListener.onMovieSelected(name);

        index = l.getFirstVisiblePosition();
        View view = listView.getChildAt(0);
        top = (view == null) ? 0 : (view.getTop() - listView.getPaddingTop());
        Log.d("index", String.valueOf(index));
        Log.d("top", String.valueOf(top));

    }

    public interface OnMovieSelectedListener {
        void onMovieSelected(String gig);
    }

}
