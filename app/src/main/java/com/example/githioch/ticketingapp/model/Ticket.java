package com.example.githioch.ticketingapp.model;

import com.parse.ParseUser;

/**
 * Created by Githioch on 6/4/2015.
 */
public class Ticket {

    private String qrCode;
    private String movieName;
    private String venue;
    private String movieDate;
    private String movieTime;
    private String selection;
    private String popcornAndSoda;


    public Ticket(String qrCode, String movieName, String venue, String movieDate,
                  String movieTime, String selection, String popcornAndSoda){

        this.qrCode = qrCode;
        this.movieName = movieName;
        this.venue = venue;
        this.movieDate = movieDate;
        this.movieTime = movieTime;
        this.selection = selection;
        this.popcornAndSoda = popcornAndSoda;

    }

    public String getQrCode(){
        return this.qrCode;
    }
    public String getMovieName(){
        return this.movieName;
    }
    public String getVenue(){
        return this.venue;
    }
    public String getMovieDate(){
        return this.movieDate;
    }
    public String getMovieTime(){
        return this.movieTime;
    }
    public String getSelection(){
        return this.movieTime;
    }
    public boolean getPopcornAndSoda(){
        return Boolean.valueOf(this.popcornAndSoda);
    }
}
