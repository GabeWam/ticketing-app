package com.example.githioch.ticketingapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.model.Movie;
import com.example.githioch.ticketingapp.model.Ticket;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 3/30/2015.
 */
public class CustomParseMyTicketsAdapter extends BaseAdapter {

    private List<ParseObject> data;
    private Context context;

    public CustomParseMyTicketsAdapter(Context context, List<ParseObject> arrayList){
        this.data = arrayList;
        this.context = context;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        if (v == null) {
            //v = View.inflate(getContext(), R.layout.list_row, null);
            v = View.inflate(context, R.layout.list_row_tickets, null);
        }

        TextView title = (TextView) v.findViewById(R.id.title_ticket);
        title.setText(data.get(position).getString("movieName"));

        TextView venue = (TextView) v.findViewById(R.id.venue_ticket);
        venue.setText(data.get(position).getString("venue"));

        TextView eventdate = (TextView) v.findViewById(R.id.eventdate_ticket);
        eventdate.setText(data.get(position).getString("movieDate"));

        TextView ticketselection = (TextView) v.findViewById(R.id.ticket_selection);
        ticketselection.setText(data.get(position).getString("selection"));

        TextView popcornandsoda = (TextView) v.findViewById(R.id.bool_popcorn_and_soda);
        if(data.get(position).getBoolean("popcornAndSoda")){
            if(popcornandsoda.getVisibility() == View.GONE){
                popcornandsoda.setVisibility(View.VISIBLE);
            }
            popcornandsoda.setText("Soda and popcorn with your ticket");
        }
        else{
            popcornandsoda.setVisibility(View.GONE);
        }

        return v;
    }
}
