package com.example.githioch.ticketingapp;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.githioch.ticketingapp.model.Movie;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 5/8/2015.
 * Tickets will be downloaded from parse and displayed as user's phone numbers
 * This activity will be used for error correction purposes e.g. searching for users
 * if scanning fails or letting users in in case scanning fails
 */
public class DisplayDownloadedTickets extends ActionBarActivity{

    private List<Movie> eventlist = new ArrayList<>();
    private ProgressWheel wheel;
    private TextView no_internet;
    private Button retry;
    private ParseQueryAdapter parseQueryAdapter;
    private CustomParseMovieGoersAdapter customParseMovieGoersAdapter;
    private String objectId;
    private String movieName;
    private ListView listView;
    private String time;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.layout_tickets_downloaded);

        movieName = getIntent().getStringExtra("movie_name");

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_tickets_downloaded);
        setSupportActionBar(mToolbar);

        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setTitle("Tickets Downloaded");

        wheel  = (ProgressWheel) findViewById(R.id.progress_wheel);

        // USING PARSE FOR GETTING INFO
        parseQueryAdapter = new ParseQueryAdapter(this,"hello");
        parseQueryAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener() {
            @Override
            public void onLoading() {
            }

            @Override
            public void onLoaded(List list, Exception e) {
                wheel.stopSpinning();
                wheel.setVisibility(View.GONE);
            }
        });

        listView = (ListView) findViewById(android.R.id.list);
        customParseMovieGoersAdapter = new CustomParseMovieGoersAdapter(getApplicationContext());
        parseQueryAdapter.loadObjects();
        listView.setAdapter(customParseMovieGoersAdapter);
        registerForContextMenu(listView);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == android.R.id.list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            ParseObject obj = (ParseObject) lv.getItemAtPosition(acmi.position);
            objectId = obj.getString("ticketQrCode");
            Log.d("objectid",objectId);

            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Check In Manually");//groupId, itemId, order, title
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getTitle() == "Check In Manually") {
            ParseQuery<ParseObject> parseObjectParseQuery  = new ParseQuery<ParseObject>("hello");
            parseObjectParseQuery.fromLocalDatastore();
            parseObjectParseQuery.whereEqualTo("ticketQrCode", objectId);
            try {
                List<ParseObject> parseObjectList =  parseObjectParseQuery.find();
                ParseObject parseObject = parseObjectList.get(0);
                parseObject.put("checkedIn", true);
                ParseQuery<ParseObject> objectParseQuery = new ParseQuery<ParseObject>
                        (parseObject.getString("tickets"));
                objectParseQuery.whereEqualTo("ticketQrCode", objectId);
                objectParseQuery.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> list, ParseException e) {
                        list.get(0).put("checkedIn", true);
                        list.get(0).saveInBackground();

                    }
                });
                parseObject.pinInBackground();
                customParseMovieGoersAdapter = new CustomParseMovieGoersAdapter
                        (getApplicationContext());
                parseQueryAdapter.loadObjects();
                listView.setAdapter(customParseMovieGoersAdapter);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
