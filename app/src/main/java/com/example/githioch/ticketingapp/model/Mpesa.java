package com.example.githioch.ticketingapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Githioch on 3/24/2015.
 */
public class Mpesa {

    private String smsbody;
    private long smsDate;

    public Mpesa() {

    }

    public void setSmsBody(String sms) {

        this.smsbody = sms;

    }

    public String getSmsbody() {

        return smsbody;
    }

    public String getFirstWordfromSms() {

        //int positionspace = smsbody.indexOf(" ");

        return smsbody.substring(0, smsbody.indexOf(" "));

    }

    public void setSmsDate(long smsDate) {
        this.smsDate = smsDate;
    }

    public String getSmsDate() {
        String formattedDate = new SimpleDateFormat("MM/dd/yyyy").format(smsDate);

        Date dt = new Date(smsDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        String time = sdf.format(dt);

        return formattedDate+ " "+ time;
    }

    //public int getConfirmationCode()
}
