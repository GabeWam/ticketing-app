package com.example.githioch.ticketingapp;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 5/31/2015.
 */
public class AnalyticsActivity extends ActionBarActivity{

    private String date;
    private ParseQueryAdapter parseQueryAdapter;
    private ListView listView;
    private CustomParseAnalyticsAdapter customParseAnalyticsAdapter;
    private   List<List<ParseObject>> arrayParseObjects = new ArrayList<List<ParseObject>>();
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);


        date = getIntent().getStringExtra("date");

        listView = (ListView) findViewById(android.R.id.list);

        setListData();

        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setTitle("Analytics");
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setDisplayShowTitleEnabled(true);
        menu.setBackgroundDrawable(getResources().getDrawable(R.color.background_material_dark));


    }

    private void setListData() {
        // search for parseobject lists
        final ParseQuery<ParseObject> parseObjectParseQuery = new ParseQuery<ParseObject>("tickets");
        parseObjectParseQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {
                List<String> movieNames = new ArrayList<String>();

                for (int i = 0; i < list.size(); i++) {
                    if(!movieNames.contains(list.get(i).getString("movieDate"))){
                        movieNames.add(list.get(i).getString("movieDate"));
                    }
                }

                Log.d("movienames size", String.valueOf(movieNames.size()));

                for (int i = 0; i < movieNames.size(); i++) {
                    ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("hello");
                    parseQuery.fromLocalDatastore();
                    parseQuery.whereEqualTo("movieDate", movieNames.get(i));
                    try {
                        List<ParseObject> list2 =  parseQuery.find();

                        arrayParseObjects.add(list2);

                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }

                }

                Log.d("array size", String.valueOf(arrayParseObjects.size()));

                customParseAnalyticsAdapter = new CustomParseAnalyticsAdapter(AnalyticsActivity.this
                        , arrayParseObjects);
                listView.setAdapter(customParseAnalyticsAdapter);

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
