package com.example.githioch.ticketingapp;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class ReimbursementFragment extends DialogFragment {
    public interface ReimbursementListener {
        public void onDialogPositiveClick(DialogFragment dialog);

    }

    public interface ReimbursementCancel {
        public void onDialogNegativeClick(DialogFragment dialog);

    }



    private String mTitle;
    private String mMessage;
    private  ReimbursementListener mListener;
    private  ReimbursementCancel mListenercancel;


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ReimbursementFragment newInstance(String title, String message, ReimbursementListener listener,
                                                       ReimbursementCancel codeCancel) {
        ReimbursementFragment fragment = new ReimbursementFragment();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.mListenercancel = codeCancel;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(mMessage)
                .setTitle(mTitle);


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mListener != null) {
                    mListener.onDialogPositiveClick(ReimbursementFragment.this);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListenercancel != null) {
                    mListenercancel.onDialogNegativeClick(ReimbursementFragment.this);
                }
            }
        });


        return builder.create();
    }
}
