package com.example.githioch.ticketingapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Githioch on 5/8/2015.
 */
public class TicketsBoughtActivity extends ActionBarActivity{

    private Button btnShowProgress;
    private Button btnDisplaydownloadedtickets;
    private Button btnStartscanningtickets;
    private ProgressDialog pDialog;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;


    private ZXingScannerView mScannerView;
    private TextView number_of_valid_tickets;
    private TextView amount_of_money_collected;
    private String name;
    private boolean downloadedticketsclicked;
    private String date;
    private String time;
    private String movie_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        setContentView(R.layout.layout_tickets_bought);

        date = getIntent().getStringExtra("date");
        time = getIntent().getStringExtra("time");
        movie_name = getIntent().getStringExtra("movie_name");

        movie_name+= "_Payments";

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("numberticketsvalid"+movie_name);
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(list.size() == 0){

                    ParseQuery<ParseObject> parseQuery2 = new ParseQuery<ParseObject>(movie_name);
                    parseQuery2.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> list, ParseException e) {

                            ParseObject.pinAllInBackground(movie_name, list, new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    Log.d("payments", "pinned");
                                }
                            });

                        }
                    });
                }
            }
        });




        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_tickets_bought);
        setSupportActionBar(mToolbar);

        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setTitle("Tickets Bought");


        // show progress bar button
        btnShowProgress = (Button) findViewById(R.id.btnProgressBar);
        btnDisplaydownloadedtickets = (Button) findViewById(R.id.btnDisplayResults);
        btnStartscanningtickets = (Button) findViewById(R.id.btnStartScanning);

        number_of_valid_tickets = (TextView) findViewById(R.id.text_number_valid);
        amount_of_money_collected = (TextView) findViewById(R.id.textview_amount_collected);

        //TODO if i decide that enabling event scanners to collaborate
        // find tickets that have already being scanned i.e. "Checked_In" is true
        /*ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("mpesaconfirmation");
        parseQuery.whereEqualTo("Checked_In", true);
        Integer number = 0;
        try {
           number = parseQuery.count();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/


        Integer number = 0;
        // set layout with number of tickets found
        number_of_valid_tickets.setText("Tickets Scanned: "+ String.valueOf(number));
        amount_of_money_collected.setText("Money Collected: "+ String.valueOf(number));

        btnDisplaydownloadedtickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadedticketsclicked = true;
                Intent intent = new Intent(getApplicationContext(), DisplayDownloadedTickets.class);
                intent.putExtra("movie_name", movie_name);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                startActivity(intent);
            }
        });

        btnStartscanningtickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // launch zxing
                Intent intent = new Intent(getApplicationContext(), QRCodeScannerActivity.class);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                intent.putExtra("movie_name", movie_name);
                startActivity(intent);
            }
        });


        /**
         * Show Progress bar click event
         * */
        btnShowProgress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // starting new Async Task
                new DownloadTicketPurchasers().execute();

            }
        });


        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(downloadedticketsclicked){
            btnDisplaydownloadedtickets.setVisibility(View.VISIBLE);
        }

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("numberticketsvalid"+movie_name);
        parseQuery.fromLocalDatastore();
        Integer number = 0;
        Integer amount = 0;
        try {
            number = parseQuery.count();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<ParseObject> list = null;
        try {
            list = parseQuery.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < list.size(); i++) {
           // getAmount(list.get(i));
            amount += list.get(i).getInt("amount");
        }

        //TODO revert implementation
        if(number != 0){
//            btnShowProgress.setVisibility(View.GONE);
            btnDisplaydownloadedtickets.setVisibility(View.VISIBLE);
            btnStartscanningtickets.setVisibility(View.VISIBLE);
        }
        Log.d("amount", String.valueOf(amount));
        number_of_valid_tickets.setText("Tickets Scanned: "+ String.valueOf(number));
        amount_of_money_collected.setText("Money Collected: "+ String.valueOf(amount));
    }


    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //slide from left to right
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class DownloadTicketPurchasers extends AsyncTask<Void,String,Object>{

        @Override
        protected Object doInBackground(Void... params) {
            ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>(movie_name);
            try {
                List<ParseObject> list = parseQuery.find();

                int size = list.size();
                long total = 0;
                for (int i = 0; i < size; i++) {
                    total += i;
                    ParseObject parseObject = list.get(i);
                    parseObject.pin();
                    /*ParseObject parseObject1 = new ParseObject("movie_goers");
                    parseObject1.put("objectId", parseObject.getObjectId());
                    parseObject1.put("phoneNumber", parseObject.getString("phoneNumber"));
                    parseObject1.put("student", parseObject.getBoolean("student"));
                    parseObject1.put("checkedIn", parseObject.getBoolean("checkedIn"));
                    parseObject1.pin();*/
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / size));
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected void onPostExecute(Object o) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            btnDisplaydownloadedtickets.setVisibility(View.VISIBLE);
            btnStartscanningtickets.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(String...progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }


    }
}
