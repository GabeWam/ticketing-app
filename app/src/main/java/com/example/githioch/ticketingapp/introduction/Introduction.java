package com.example.githioch.ticketingapp.introduction;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.adapter.TextSliderViewMine;

import java.util.ArrayList;

import javax.xml.transform.Transformer;

/**
 * Created by Githioch on 6/28/2015.
 */
public class Introduction extends ActionBarActivity {

    private SliderLayout sliderShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_test);

        sliderShow = (SliderLayout) findViewById(R.id.slider);
        sliderShow.stopAutoCycle();
        sliderShow.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

        ArrayList<Integer> integerArrayList = new ArrayList<>();
        integerArrayList.add(R.drawable.one);
        integerArrayList.add(R.drawable.two);
        integerArrayList.add(R.drawable.three);
        integerArrayList.add(R.drawable.four);
        integerArrayList.add(R.drawable.five);
        integerArrayList.add(R.drawable.six);
        integerArrayList.add(R.drawable.seven);
        integerArrayList.add(R.drawable.eight);
        integerArrayList.add(R.drawable.nine);
        integerArrayList.add(R.drawable.ten);

        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("Select movie");
        stringArrayList.add("Select movie details");
        stringArrayList.add("Select MPESA");
        stringArrayList.add("Pick MPESA");
        stringArrayList.add("Select 'Lipa na MPESA'");
        stringArrayList.add("Select 'Buy Goods And Services'");
        stringArrayList.add("Enter till no shown at the top, send payment and await MPESA confirmation");
        stringArrayList.add("Go back to the movie and pick 'Get Confirmation Code'");
        stringArrayList.add("Select confirmation code for payment just made");
        stringArrayList.add("That's it! Go with your phone to the cinema!");

        for (int i = 0; i < integerArrayList.size(); i++) {

            TextSliderViewMine textSliderView = new TextSliderViewMine(this);
            textSliderView
                    .description(stringArrayList.get(i))
                    .image(integerArrayList.get(i));

            sliderShow.addSlider(textSliderView);
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        sliderShow.stopAutoCycle();
    }

}
