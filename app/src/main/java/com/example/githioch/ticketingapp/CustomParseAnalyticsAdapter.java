package com.example.githioch.ticketingapp;

import android.app.Activity;
import android.content.Context;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Githioch on 5/31/2015.
 */

//TODO money collected
public class CustomParseAnalyticsAdapter extends BaseAdapter {


    private Activity activity;
    private Context context;
    private List<List<ParseObject>> data;
    private static LayoutInflater inflater = null;
    private List<ParseObject> tempValues;

    public CustomParseAnalyticsAdapter(Activity a, List<List<ParseObject>> d) {

        /********** Take passed values **********/
        activity = a;
        data = d;



        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView movieTitle;
        public TextView numberOfTickets;
        public TextView numberScanned;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.tabitem, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.movieTitle = (TextView) view.findViewById(R.id.analytics_movie_name);
            holder.numberOfTickets = (TextView) view.findViewById(R.id.analytics_number_of_tickets);
            holder.numberScanned = (TextView) view.findViewById(R.id.analytics_number_scanned);

            /************  Set holder with LayoutInflater ************/
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        if (data.size() <= 0) {
            holder.movieTitle.setText("No Data");

        } else {
            /***** Get each Model object from Arraylist ********/
            tempValues = null;

            tempValues = data.get(position);
            Log.d("position", String.valueOf(position));
            Log.d("size again", String.valueOf(data.size()));
            Log.d("temp", String.valueOf(tempValues.size()));


            /************  Set Model values in Holder elements ***********/
            String moviename = tempValues.get(0).getString("movieName");
            holder.movieTitle.setText(moviename.replaceAll("_", " "));

            holder.numberOfTickets.setText("Total number of tickets: "+ String.valueOf(tempValues.size()));

            int numberscanned = 0;
            for (int i = 0; i < tempValues.size(); i++) {
                if (tempValues.get(i).getBoolean("checkedIn")) {
                    numberscanned++;
                }
            }


            holder.numberScanned.setText("Tickets scanned: " + String.valueOf(numberscanned));

            /******** Set Item Click Listner for LayoutInflater for each row *******/
        }
        return view;
    }
}


