package com.example.githioch.ticketingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Githioch on 3/24/2015.
 */
public class ConfirmPaymentActivity extends FragmentActivity {

    private String ticket_selection;
    private boolean successful_payment;
    private String result;
    private boolean success;
    private String ticketSelection;
    private String movieDate;
    private String movieTime;
    private String ticketCost;
    private boolean popcornAndSoda;
    private String venue;


    public void setPaymentState(boolean success){
        this.success = success;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_confirm_payment);

        String confirmationCode = getIntent().getExtras().getString("confirmationCode");
        String movieName = getIntent().getStringExtra("movieName");
        ticketSelection = getIntent().getStringExtra("ticketSelection");
        movieDate = getIntent().getStringExtra("movieDate");
        movieTime = getIntent().getStringExtra("movieTime");
        ticketCost = getIntent().getStringExtra("ticketCost");
        popcornAndSoda = getIntent().getBooleanExtra("popcornAndSoda", false);
        venue = getIntent().getStringExtra("venue");


        // calling fragment
        if (savedInstanceState == null) {

            PaymentResultFragment paymentresultFragment
                    = new PaymentResultFragment();

            Bundle bundle = new Bundle();
            bundle.putString("movieName", movieName);
            bundle.putString("ticketSelection", ticketSelection);
            bundle.putString("confirmationCode", confirmationCode);
            bundle.putString("movieTime", movieTime);
            bundle.putString("movieDate", movieDate);
            bundle.putString("ticketCost", ticketCost);
            bundle.putBoolean("popcornAndSoda", popcornAndSoda);
            bundle.putString("venue", venue);

            paymentresultFragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.frame_payment, paymentresultFragment);
           // transaction.addToBackStack(null);

            transaction.commit();

        }
    }

    @Override
    public void onBackPressed() {
        if(this.success) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        super.onBackPressed();
    }
}
