package com.example.githioch.ticketingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 5/26/2015.
 */
public class MovieTimesActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    private String movieName;
    private ListView listView;
    private ProgressWheel wheel;
    private TextView no_internet;
    private Button retry;
    private ParseQueryAdapter parseQueryAdapter;
    private Spinner dropdowndateselection;
    private Spinner dropdowntimeselection;
    private List<ParseObject> parseObjectList;
    private String date_selected;
    private String time_selected;
    private Button okay;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_times_layout);

        if(getIntent().getStringExtra("movie_name")!= null) {

            movieName = getIntent().getStringExtra("movie_name").replaceAll(" ", "_");


        }


        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setTitle("Movie Times");
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setDisplayShowTitleEnabled(true);
        menu.setBackgroundDrawable(getResources().getDrawable(R.color.background_material_dark));

        listView = (ListView) findViewById(android.R.id.list);
        wheel =(ProgressWheel) findViewById(R.id.progress_wheel);
        no_internet = (TextView) findViewById(R.id.no_internet);
        retry = (Button) findViewById(R.id.retry);
        okay = (Button) findViewById(R.id.btn_movie_times_ok);

        dropdowndateselection = (Spinner)findViewById(R.id.spinner_date_selection_manager);
        dropdowntimeselection = (Spinner)findViewById(R.id.spinner_time_selection_manager);

        dropdowndateselection.setOnItemSelectedListener(this);
        dropdowntimeselection.setOnItemSelectedListener(this);

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>(movieName);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("movie times", "loaded");
                parseObjectList = list;
                List<Object> arraylistdates = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    String date = list.get(i).getString("movieDate");
                    arraylistdates.add(i, date);
                }

                String[] itemsdates = arraylistdates.toArray(new String[arraylistdates.size()]);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_row, itemsdates);
                dropdowndateselection.setAdapter(adapter);

                wheel.stopSpinning();
                wheel.setVisibility(View.GONE);
            }
        });



        ConnectionDetector connectionDetector = new ConnectionDetector(this);
        final boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), TicketsBoughtActivity.class);
                intent.putExtra("movie_name", movieName);
                intent.putExtra("date", date_selected);
                intent.putExtra("time", time_selected);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch(parent.getId()) {

            case R.id.spinner_date_selection_manager:

                List<Object> arraylisttimes = new ArrayList<>();
                arraylisttimes = (List<Object>) parseObjectList.get(position).get("movieTimes");

                String[] itemstimes = arraylisttimes.toArray(new String[arraylisttimes.size()]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_row, itemstimes);
                dropdowntimeselection.setAdapter(adapter);

                date_selected = parent.getItemAtPosition(position).toString();

                break;

            case R.id.spinner_time_selection_manager:

                time_selected = parent.getItemAtPosition(position).toString();

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
