package com.example.githioch.ticketingapp;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.adapter.CustomSmsAdapter;
import com.example.githioch.ticketingapp.model.Mpesa;
import com.example.githioch.ticketingapp.util.Util;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Githioch on 3/24/2015.
 */
public class MpesaListActivity extends FragmentActivity implements
        ConfirmationCodeFragment.ConfirmationCodeListener,
        AdapterView.OnItemClickListener, ConfirmationCodeFragment.ConfirmationCodeCancel,
        ReimbursementFragment.ReimbursementListener, ReimbursementFragment.ReimbursementCancel {

    // Log tag
    private static final String TAG = MpesaListActivity.class.getSimpleName();

//    private static final String MPESA_PHONE_NUMBER = "MPESA";
    private static final String MPESA_PHONE_NUMBER =  "+16152366094";
//    private static final String MPESA_PHONE_NUMBER =  "+1615236566094";

    String eventname;
    String firstword;
    DialogFragment dialogFragment;
    private List<Mpesa> mpesaSmsList = new ArrayList<>();
    private String eventcost;
    private CustomSmsAdapter customSmsAdapter;
    private String ticket_selection;
    private String date_selected;
    private String movieName;
    private String ticketSelection;
    private String dateSelected;
    private String timeSelected;
    private String ticketCost;
    private TextView noMpesa;
    private boolean reimburse;
    private String code;
    private Double amount;
    private boolean popcornAndSoda;
    private String venue;


    //TODO only display sms from Safaricom confirming money was sent
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_mpesa_sms);

        movieName = getIntent().getExtras().getString("movieName");
        ticketSelection = getIntent().getExtras().getString("ticketSelection");
        dateSelected = getIntent().getStringExtra("movieDate");
        timeSelected = getIntent().getStringExtra("movieTime");
        ticketCost = getIntent().getStringExtra("ticketCost");
        popcornAndSoda = getIntent().getBooleanExtra("popcornAndSoda", false);
        venue = getIntent().getStringExtra("venue");


        ListView listView = (ListView) findViewById(android.R.id.list);
        noMpesa = (TextView) findViewById(R.id.no_mpesa);

        listView.setOnItemClickListener(this);

        if(getIntent() != null){
            reimburse = getIntent().getBooleanExtra("reimburse", false);
            Log.d("reimburse", String.valueOf(reimburse));
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // fetch sms from user's phone matching a particular parameter
                fetchMpesaSmsFromUsersPhoneWithStringReceived();

            }
        });

        Log.d("mepsalist ", String.valueOf(mpesaSmsList.size()));
        if(mpesaSmsList.size() == 0){
            noMpesa.setVisibility(View.VISIBLE);
        }
        else{
            customSmsAdapter = new CustomSmsAdapter(this, mpesaSmsList);
            listView.setAdapter(customSmsAdapter);

            customSmsAdapter.notifyDataSetChanged();
        }

    }

    private void fetchMpesaSmsFromUsersPhoneWithStringReceived() {

        StringBuilder smsBuilder = new StringBuilder();
        final String SMS_URI_INBOX = "content://sms/inbox";
        final String SMS_URI_ALL = "content://sms/";

        try {
            Uri uri = Uri.parse(SMS_URI_INBOX);
            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
            Cursor cur = getContentResolver().query(uri, projection, "address='"
                    + MPESA_PHONE_NUMBER + "'", null, null);
            if (cur.moveToFirst()) {
                int index_Body = cur.getColumnIndex("body");
                int index_Date = cur.getColumnIndex("date");

                do {
                    String strbody = cur.getString(index_Body);

                    long longDate = cur.getLong(index_Date);

                    if(strbody.contains("received")){

                        Mpesa mpesa = new Mpesa();
                        mpesa.setSmsBody(strbody);
                        mpesa.setSmsDate(longDate);

                        mpesaSmsList.add(mpesa);
                    }


                } while (cur.moveToNext());

                if (!cur.isClosed()) {
                    cur.close();
                    cur = null;
                }
            } else {
                smsBuilder.append("no result!");
            } // end if

        } catch (SQLiteException ex) {
            Log.d("SQLiteException", ex.getMessage());
        }
    }


    private Double getAmountPaid(String messagebody_cloud) {

        // save each word in an arraylist and get seventh word (assuming this is
        // the word containing the amount

        List<String> tokens = new ArrayList<String>();

        String text = messagebody_cloud;
        StringTokenizer st = new StringTokenizer(text);

        //("---- Split by space ------");
        while (st.hasMoreElements()) {
            tokens.add(st.nextElement().toString());
        }

        //TODO change back to get(2)
        String amountpaid = tokens.get(5);
        Double numberOnly= Double.valueOf(amountpaid.replaceAll("[^\\.0-9]", ""));

        return numberOnly;


    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        if(reimburse){
            Util util = new Util(getApplicationContext());
            String phoneNumber = util.findPhoneNumber();

            ParseObject parseObject = new ParseObject("Reimbursement_Requests");
            parseObject.put("phoneNumber", phoneNumber);
            parseObject.put("confirmationCode", code);
            parseObject.put("amountToReimburse", amount);
            parseObject.saveInBackground();

            Toast.makeText(getApplicationContext(), "Request sent", Toast.LENGTH_LONG)
                    .show();
            finish();
        }
        else{

            Intent intent = new Intent(this, ConfirmPaymentActivity.class);
            intent.putExtra("confirmationCode", firstword);
            intent.putExtra("movieName", movieName);
            intent.putExtra("ticketSelection", ticketSelection);
            intent.putExtra("movieDate", dateSelected);
            intent.putExtra("movieTime", timeSelected);
            intent.putExtra("ticketCost",ticketCost);
            intent.putExtra("popcornAndSoda", popcornAndSoda);
            intent.putExtra("venue",venue);
            startActivity(intent);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Mpesa mpesa = (Mpesa) customSmsAdapter.getItem(position);
        firstword = mpesa.getFirstWordfromSms();
        Log.d("firstword", firstword);

        //mConfirmationListener.onConfirmationSelected(firstword);

        amount = getAmountPaid(mpesa.getSmsbody());
        code = mpesa.getFirstWordfromSms();

        if(reimburse){
            dialogFragment = ReimbursementFragment.newInstance("Confirm","Note: Reimbursement " +
                    "requests will not be accepted within 2 hours of the movie.\nAre you sure Ksh." +
                    amount + " is the correct payment you want reimbursed?", this, this);
            dialogFragment.show(getSupportFragmentManager(), "confirm");
        }
        else{
            dialogFragment = ConfirmationCodeFragment.newInstance("Confirm", "Are you sure Ksh." +
                    amount + " is the correct payment sent for the movie?", this, this);
            dialogFragment.show(getSupportFragmentManager(), "confirm");
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
    }
}

