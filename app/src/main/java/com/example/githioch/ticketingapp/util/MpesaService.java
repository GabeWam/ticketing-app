package com.example.githioch.ticketingapp.util;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Githioch on 3/22/2015.
 */
public class MpesaService extends Service {

    Intent Action;
    private TimerTask mDoTask;
    private Timer mT = new Timer();

    public IBinder onBind(Intent paramIntent) {
        return null;
    }

    public void onCreate() {
        this.Action = new Intent();
        this.Action.setAction("ping");
        super.onCreate();
        this.mDoTask = new TimerTask() {
            public void run() {
                sendBroadcast(Action);
            }
        };
        this.mT.scheduleAtFixedRate(this.mDoTask, 1000L, 500L);
    }

    public void onDestroy() {
        super.onDestroy();
        this.mT.cancel();
    }
}

