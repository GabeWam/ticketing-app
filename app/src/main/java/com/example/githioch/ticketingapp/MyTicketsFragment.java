package com.example.githioch.ticketingapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.githioch.ticketingapp.adapter.CustomParseMyTicketsAdapter;
import com.example.githioch.ticketingapp.model.Ticket;
import com.example.githioch.ticketingapp.util.Util;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Githioch on 4/1/2015.
 */
public class MyTicketsFragment extends ListFragment {

    private static final String KEY_EVENT_NAME = "Event_Name";
    private static final String KEY_EVENT_VENUE = "Venue";
    private static final String KEY_EVENT_DATE = "Event_Date";
    private static final String KEY_EVENT_DESCRIPTION = "Event_Description";
    private static final String KEY_EVENT_IMAGE = "imagetest";
    private static final String KEY_EVENT_QRCODE_DATA = "qrcodedata";
    // Log tag
    private final String TAG_MY_TICKETS_FRAGMENT = MyTicketsFragment.class.getSimpleName();
    ParseQueryAdapter parseQueryAdapter;
    ParseQuery<ParseObject> parseObjectParseQuery;
    private ListView listView;
    private CustomParseMyTicketsAdapter customParseAdapter;
    private ParseQueryAdapter.QueryFactory<ParseObject> factory;
    private OnTicketSelectedListener mListener;

    private String qrcode = "";
    private TextView notloggedin;
    private Button login;
    private String phoneNumber;
    private ProgressWheel wheel;
    private TextView noTicketsFound;
    private ArrayList<String> stringList;
    private ArrayList ticketList;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnTicketSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnEventSelectedListener");
        }
    }


    private void retrieveTickets() {

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("tickets");
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {

                if(list.size() > 0){
                    Log.d("ticket list",String.valueOf(list.size()));
                    customParseAdapter = new CustomParseMyTicketsAdapter(getActivity()
                            .getApplicationContext(), list);
                    listView.setAdapter(customParseAdapter);
                    wheel.stopSpinning();
                    wheel.setVisibility(View.GONE);
                }else{

                    Util util = new Util(getActivity());
                    ParseQuery<ParseObject> parseQuery1 = new ParseQuery<ParseObject>("tickets");
                    parseQuery1.whereEqualTo("phoneNumber", util.findPhoneNumber());
                    parseQuery1.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> list, ParseException e) {

                            if(list.size() > 0){
                                Log.d("ticket list1",String.valueOf(list.size()));
                                customParseAdapter = new CustomParseMyTicketsAdapter(getActivity()
                                        .getApplicationContext(), list);
                                listView.setAdapter(customParseAdapter);
                                wheel.stopSpinning();
                                wheel.setVisibility(View.GONE);
                                ParseObject.pinAllInBackground(list);
                            }else{
                                Log.d("user", "has no tickets");
                                noTicketsFound.setVisibility(View.VISIBLE);
                                wheel.stopSpinning();
                                wheel.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_my_tickets, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);

        notloggedin = (TextView) rootView.findViewById(R.id.not_logged_in);
        login = (Button) rootView.findViewById(R.id.login);
        wheel = (ProgressWheel) rootView.findViewById(R.id.progress_wheel);
        noTicketsFound = (TextView) rootView.findViewById(R.id.no_tickets_found);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                startActivity(intent);
            }
        });

        if(ParseUser.getCurrentUser() != null){

//            new ticketsThread().execute();
            retrieveTickets();

        }
        else{
            wheel.stopSpinning();
            wheel.setVisibility(View.GONE);
            notloggedin.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
        }



        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // will open new fragment and display qr code

        CustomParseMyTicketsAdapter customParseMyTicketsAdapter = (CustomParseMyTicketsAdapter) l.getAdapter();
        ParseObject ticket = (ParseObject) customParseMyTicketsAdapter.getItem(position);
        String qr = ticket.getString("ticketQrCode");
        Log.d("qr", qr);

        mListener.onTicketSelected(qr);

    }

    public interface OnTicketSelectedListener {
        void onTicketSelected(String gig);
    }

    private class ticketsThread extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (stringList != null) {

                customParseAdapter = new CustomParseMyTicketsAdapter(getActivity()
                        .getApplicationContext(), ticketList);
                listView.setAdapter(customParseAdapter);
                wheel.stopSpinning();
                wheel.setVisibility(View.GONE);
            } else {
                Log.d("user", "has no tickets");
                noTicketsFound.setVisibility(View.VISIBLE);
                wheel.stopSpinning();
                wheel.setVisibility(View.GONE);
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            retrieveTickets();
            return null;
        }
    }
}
