package com.example.githioch.ticketingapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Githioch on 3/24/2015.
 */
public class ConfirmPayment extends Fragment {

    private static String confirmationcode;

    private static String event_name;
    TextView confirmationcodefield;
    Button pay;
    private Double eventCost;
    private String emailaddress;
    private String objectid;
    private String payment_id;
    private String ticketSelection;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_confirm_payment_details, container, false);
        confirmationcodefield = (TextView) rootView.findViewById(R.id.confirmationcodetextview);
        pay = (Button) rootView.findViewById(R.id.pay_button);
        confirmationcodefield.setText(getConfirmationCode());
        final TextView no_internet = (TextView) rootView.findViewById(R.id.no_internet);
        final LinearLayout main = (LinearLayout) rootView.findViewById(R.id.frame_payment_details);
        final Button retry = (Button) rootView.findViewById(R.id.retry);

        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        final Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isInternetPresent) {
                    // query looks for all confirmed payments from user
                    paymentResult();

                    // DEFAULT IS FALSE IN PARSE

                } else {
                    retry.setVisibility(View.VISIBLE);
                    pay.setVisibility(View.GONE);
                    main.setVisibility(View.GONE);
                    no_internet.setVisibility(View.VISIBLE);

                }
            }

        });

        return rootView;
    }

    /**
     * Tells user that the payment is successful and creates qr code that is stored
     * in user's phone
     *
     *
     */
    private void paymentResult() {

        PaymentResultFragment paymentresultFragment
                = new PaymentResultFragment();

        paymentresultFragment.setEventName(this.event_name);
        paymentresultFragment.setTicketSelection(getTicketSelection());
        paymentresultFragment.setConfirmationCode(getConfirmationCode());
        Log.d("ticket", getTicketSelection());



        FragmentTransaction transaction = getActivity().
                getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.frame_payment, paymentresultFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();


    }

    private void sendConfirmationEmail(String address) {
        String addressnewstring = address.substring(1, address.length());
        Log.d("addressnew", addressnewstring);

        Number addressnew = Long.parseLong(addressnewstring);
        // Log.d("numner", String.valueOf(addressnew));

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("_User");
        parseQuery.whereEqualTo("phone_number", addressnew);
        // TODO is this the best way to retrieve one object??
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() == 0) {
                    Log.d("list", "empty");
                }
            }
        });

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("to", getEmailaddress());
        // objectid being used as orderid
        params.put("orderid", getObjectid());
        ParseCloud.callFunctionInBackground("orderConfirmation", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
                if (e == null) {
                    // result is "Hello world!"
                    Log.d("sendgrid", result);
                }
            }
        });


    }


    private String getConfirmationCode() {
        return confirmationcode;
    }

    public String getEmailaddress() {
        return this.emailaddress;
    }


    public String getObjectid() {
        return this.objectid;
    }

    public String getTicketSelection() {
        return this.ticketSelection;
    }
}
