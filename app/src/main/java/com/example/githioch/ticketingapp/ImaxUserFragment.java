package com.example.githioch.ticketingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.example.githioch.ticketingapp.model.Movie;
import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 3/6/2015.
 * Imax Users will be able to view this page after logging in like every other user
 * Here they will be able to see the movies they can check people in for
 */
//TODO when resuming to this page after downloading tickets, show that tickets have been downloaded
public class ImaxUserFragment extends DialogFragment {
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    private String username_signin_member = null;
    private String password_signin_member = null;
    private String jsonresult_signin_member;
    private Button webview_button = null;
    private String response = null;
    private List<Movie> eventlist = new ArrayList<>();
    private ProgressWheel wheel;
    private TextView no_internet;
    private Button retry;
    private PullRefreshLayout layout;
    private ParseQueryAdapter parseQueryAdapter;
    private ListView listView;
    private Button btnscantickets;
    private Spinner spinnerdateselection;
    private Button btnviewanalytics;
    private String[] itemstimes;
    private Button btnscantickets_OK;
    private Button btnviewtickets;
    private Button btndownloadtickets;
    private Button btnShowProgress;
    private ProgressDialog pDialog;
    private String dateselection;
    private LinearLayout mainLayout;
    private ConnectionDetector connectionDetector;



    private void downloadValidMovieDates() {

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("Admin_Valid_Movie_Dates");
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {

                List<Object> arrayListTimes = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    arrayListTimes.add(i, list.get(i).getString("movieDates"));
                }
                itemstimes = arrayListTimes.toArray(new String[arrayListTimes.size()]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity()
                        .getApplicationContext(),
                        R.layout.spinner_row, itemstimes);
                spinnerdateselection.setAdapter(adapter);

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_imax_user, container, false);
        connectionDetector = new ConnectionDetector(getActivity());
        mainLayout = (LinearLayout)rootView.findViewById(R.id.imax_main_layout);
        no_internet = (TextView) rootView.findViewById(R.id.no_internet);
        retry = (Button) rootView.findViewById(R.id.retry);
        btndownloadtickets = (Button) rootView.findViewById(R.id.btn_download_tickets);
        btnscantickets = (Button)rootView.findViewById(R.id.btn_scan_tickets);
        spinnerdateselection = (Spinner) rootView.findViewById(R.id.spinner_admin_select_date);
        btnviewanalytics = (Button) rootView.findViewById(R.id.btn_view_analytics);
        btnscantickets_OK = (Button) rootView.findViewById(R.id.btn_scan_tickets_ok);
        btnviewtickets = (Button) rootView.findViewById(R.id.btn_view_tickets);

        boolean isInternetPresent = connectionDetector.isConnectingToInternet();
        if(isInternetPresent){
            downloadValidMovieDates();
        }else{
            mainLayout.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
            retry.setVisibility(View.VISIBLE);
        }

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionDetector = new ConnectionDetector(getActivity());
                boolean isInternetPresent = connectionDetector.isConnectingToInternet();
                if(isInternetPresent){
                    downloadValidMovieDates();
                    no_internet.setVisibility(View.GONE);
                    retry.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                }

            }
        });


        btndownloadtickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // starting new Async Task
                DownloadTicketPurchasers downloadTicketPurchasers = new DownloadTicketPurchasers();
                downloadTicketPurchasers.execute();

            }
        });

        btnscantickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), QRCodeScannerActivity.class);
                intent.putExtra("date", dateselection);
                startActivity(intent);

            }
        });

        spinnerdateselection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dateselection = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnviewtickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        DisplayDownloadedTickets.class);
                startActivity(intent);
            }
        });

        btnviewanalytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        AnalyticsActivity.class);
                intent.putExtra("date", dateselection);
                startActivity(intent);
            }
        });

        return rootView;
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        android.support.v7.app.ActionBar menu = ((ActionBarActivity)getActivity()).
                getSupportActionBar();
        menu.setDisplayShowTitleEnabled(true);
    }



    private class DownloadTicketPurchasers extends AsyncTask<Void,String,Object> {

        @Override
        protected Object doInBackground(Void... params) {
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("hello");
            parseQuery.fromLocalDatastore();
            parseQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(final List<ParseObject> list, ParseException e) {
                    ParseObject.unpinAllInBackground(list, new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("deleted", String.valueOf(list.size()));
                        }
                    });
                }
            });

            try {

                long total = 0;

                ParseQuery<ParseObject> parseQuery3 = new ParseQuery<ParseObject>("tickets");
                parseQuery3.whereEqualTo("movieDate", dateselection);
                List<ParseObject> objectList = parseQuery3.find();

                for (int i1 = 0; i1 < objectList.size(); i1++) {
                    ParseObject parseObject = new ParseObject("hello");

                    parseObject.put("phoneNumber", objectList.get(i1).getString("phoneNumber"));
                    parseObject.put("ticketQrCode", objectList.get(i1).getString("ticketQrCode"));
                    parseObject.put("movieName", objectList.get(i1).getString("movieName"));
                    parseObject.put("movieDate", objectList.get(i1).getString("movieDate"));
                    parseObject.put("movieTime", objectList.get(i1).getString("movieTime"));
                    parseObject.put("selection", objectList.get(i1).getString("selection"));
                    parseObject.put("popcornAndSoda",objectList.get(i1).getBoolean("popcornAndSoda"));
                    parseObject.put("checkedIn", objectList.get(i1).getBoolean("checkedIn"));

                    parseObject.pin();

                    total++;

                    publishProgress("" + (int) ((total * 100) / objectList.size()));
                    }

            }catch (ParseException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Downloading file. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.show();
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    DownloadTicketPurchasers.this.cancel(true);
                }
            });

        }

        @Override
        protected void onPostExecute(Object o) {
            // dismiss the dialog after the file was downloaded

            pDialog.dismiss();
            btnviewtickets.setVisibility(View.VISIBLE);
            btnscantickets_OK.setVisibility(View.VISIBLE);
            btnscantickets.setVisibility(View.VISIBLE);
            btnviewanalytics.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onProgressUpdate(String...progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }


    }
}
