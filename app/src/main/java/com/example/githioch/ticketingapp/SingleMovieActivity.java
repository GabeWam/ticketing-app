package com.example.githioch.ticketingapp;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RSIllegalArgumentException;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.adapter.CustomParseAdapter;
import com.example.githioch.ticketingapp.model.Movie;
import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.TextViewHirakaku;
import com.example.githioch.ticketingapp.util.TypeFaces;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Githioch on 4/5/2015.
 */
public class SingleMovieActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    //TODO returning from mpesa should have selections set and not erased
    //TODO logging in from anywhere here should update drawer

    private static String argstringsingle = "";
    // Log tag
    private final String TAG_SINGLE_EVENT_FRAGMENT = SingleMovieActivity.class.getSimpleName();
    private final int QR_CODE_WIDTH = 500;
    private final int QR_CODE_HEIGHT = 500;
    Movie event_details = new Movie();
    private TextViewHirakaku event_name;
    private TextView event_date;
    private TextView event_description;
    private Button launch_scanner;
    private ImageView imageview;
    private Button choosempesaconfirmationcode;
    private boolean check;
    private String eventcost;
    private ParseObject parseObject;
    private Button btnLogout;
    private CustomParseAdapter adapter;
    private ParseQueryAdapter.QueryFactory<ParseObject> factory;
    private ParseQueryAdapter<ParseObject> queryAdapter;
    private String ARG_STRING = null;
    private boolean checkbig;
    private ParseImageView eventimage;
    private Bitmap bitmap;
    private ImageView venueimage;
    private ImageView eventdateimage;
    private Activity activity;

    private DialogFragment dialogFragment;
    private ProgressWheel wheel;
    private String ticketselection;
    private Button retry;
    private Spinner spinner1;
    private String time_selected;
    private String student_or_regular;
    private Spinner dropdowntimeselection;
    private Spinner dropdown;
    private List<ParseObject> parseobjectslist;
    private Spinner dropdowndateselection;
    private Spinner dropdowntickettypeselection;
    private String date_selected;
    private int time_selected_position;
    private String cost;
    private TextView moviecost;
    private double ticketCost;
    private TimerTask mDoTask;

    private Timer mT = new Timer();
    private Toast toast;
    private Intent ServiceIntent;
    private View layout;
    private TextView toasttext;
    private boolean ReceiverON = false;
    private Context context;
    private ActivityManager activityManager;
    private IntentFilter filter;
    private CheckBox checkboxpopcornandmovie;
    private String venue;
    private TextView noInternet;
    private LinearLayout mainLayout;
    private ParseQuery<ParseObject> parseQuery;
    private ParseQuery<ParseObject> parseQuery1;
    private boolean twodOnly = false;
    private ParseQuery<ParseObject> parseObjectParseQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.filter = new IntentFilter("ping");
        this.context = this;

        setContentView(R.layout.activity_movie_details);
        android.support.v7.app.ActionBar menu = getSupportActionBar();
        menu.setTitle("Movie Details");
        menu.setDisplayHomeAsUpEnabled(true);
        menu.setDisplayShowTitleEnabled(true);
        menu.setBackgroundDrawable(getResources().getDrawable(R.color.background_material_dark));

        argstringsingle = getIntent().getStringExtra("gig");

        eventimage = (ParseImageView) findViewById(R.id.event_image);
        event_name = (TextViewHirakaku) findViewById(R.id.fragment_details_event_name);
        event_description = (TextView) findViewById(R.id.fragment_details_event_description);
        wheel =(ProgressWheel) findViewById(R.id.progress_wheel);
        noInternet = (TextView) findViewById(R.id.no_internet);
        mainLayout = (LinearLayout) findViewById(R.id.main_layout);
        retry = (Button) findViewById(R.id.retry);
        moviecost = (TextView) findViewById(R.id.movie_cost);
        checkboxpopcornandmovie = (CheckBox) findViewById(R.id.checkbox_popcorn_and_movie);
        dropdowndateselection = (Spinner)findViewById(R.id.spinner_date_selection);
        dropdowntimeselection = (Spinner)findViewById(R.id.spinner_time_selection);
        dropdowntickettypeselection = (Spinner) findViewById(R.id.spinner_ticket_type_selection);
        Button gotompesa = (Button) findViewById(R.id.gotompesa);
        choosempesaconfirmationcode = (Button) findViewById(R.id.choosempesaconfirmationccde);

        dropdowndateselection.setOnItemSelectedListener(this);
        dropdowntimeselection.setOnItemSelectedListener(this);
        dropdowntickettypeselection.setOnItemSelectedListener(this);

        checkboxpopcornandmovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //code to check if this checkbox is checked!
                if(ticketselection.equals("unset")){
                    CheckBox checkBox = (CheckBox) v;
                    checkBox.setChecked(false);
                    Toast.makeText(getApplicationContext(), "Please Select Ticket Type first",Toast.LENGTH_SHORT)
                            .show();
                }
                else{
                    CheckBox checkBox = (CheckBox) v;
                    Double costdouble = Double.parseDouble(moviecost.getText().toString());
                    if (checkBox.isChecked()) {
                        moviecost.setText(String.valueOf(costdouble + 100));
                    } else {
                        moviecost.setText(String.valueOf(costdouble - 100));
                    }
                }
            }
        });

        String[] tickettypes = null;
        if(twodOnly){
            tickettypes = new String[]{"unset","2D"};
        }else{
            tickettypes = new String[]{"unset","2D", "3D"};
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_row, tickettypes);
        dropdowntickettypeselection.setAdapter(adapter2);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
                boolean isInternetPresent = connectionDetector.isConnectingToInternet();
                retryFunction(isInternetPresent);
            }
        });

        // open stk but for testing purpose we are opening whatsapp
        gotompesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ticketselection.equals("unset")) {
                    Toast.makeText(getApplicationContext(), "Please select a ticket type", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    ParseUser parseUser = ParseUser.getCurrentUser();
                    if (parseUser != null) {
                        openSTKAndShowTillNumber();
                    } else {
                        // show the signup or login screen
                        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                        intent.putExtra("activity", "single_movie");
                        startActivity(intent);
                    }

                }
            }

        });

    }

    private void openSTKAndShowTillNumber() {
        try {
            Intent overlay = new Intent(getApplicationContext(), TumaMpesaOverlay.class);
            overlay.putExtra("send_name","Mpesa Ksh. "+ cost +" to till No 123456");
            startService(overlay);

            Intent intent = new Intent();
            intent.addFlags(268435456);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setAction("android.intent.action.MAIN");
            intent.setType("text/plain");
            intent.setComponent(new ComponentName("com.android.stk", "com.android.stk.StkLauncherActivity"));

            if (intent != null) {
                startActivity(intent);
            }
        } catch (ActivityNotFoundException localActivityNotFoundException) {
            Toast.makeText(getApplicationContext(), "Activity not found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //slide from left to right
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try
        {
            if ((this.ReceiverON))
            {
                this.ReceiverON = false;
                if (this.toast != null)
                {
                    this.mDoTask.cancel();
                    this.toast.cancel();
                }
                if (startService(this.ServiceIntent) != null) {
                   stopService(this.ServiceIntent);
                }
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (localIllegalArgumentException.getMessage().contains("Receiver null")) {}
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch(parent.getId()){

            case R.id.spinner_date_selection:

                List<Object> arraylisttimes = new ArrayList<>();
                try{
                    arraylisttimes = (List<Object>) parseobjectslist.get(position).get("movieTimes");
                }catch (ClassCastException classcastexception){
                    Toast.makeText(getApplicationContext(), "Error found please try again later.",
                            Toast.LENGTH_SHORT).show();
                }
                String[] itemstimes = arraylisttimes.toArray(new String[arraylisttimes.size()]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_row, itemstimes);
                dropdowntimeselection.setAdapter(adapter);

               date_selected =  parent.getItemAtPosition(position).toString();

                break;

            case R.id.spinner_time_selection:

                String[] tickettypes = null;
                if(twodOnly){
                    Log.d("2d", "passed");
                    tickettypes = new String[]{"unset","2D"};
                }else{
                    tickettypes = new String[]{"unset","2D", "3D"};
                }

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_row, tickettypes);
                dropdowntickettypeselection.setAdapter(adapter2);

                time_selected = parent.getItemAtPosition(position).toString();
                time_selected_position =  position;

                break;

            case R.id.spinner_ticket_type_selection:
                if(position == 1){
                    for(int i = 0; i < parseobjectslist.size(); i++){
                        if(parseobjectslist.get(i).getString("movieDate") == date_selected){
                            List<Object> arrayliststudentcosts = new ArrayList<>();
                            try{
                                arrayliststudentcosts = (List<Object>) parseobjectslist.get(i)
                                        .get("twodCost");
                            }catch (ClassCastException classcastexception){
                                Toast.makeText(getApplicationContext(), "Error found please try again later.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            cost = (String) arrayliststudentcosts.get(time_selected_position);
                            if(checkboxpopcornandmovie.isChecked()){
                                moviecost.setText(String.valueOf(Double.parseDouble(cost)
                                        + 100));
                            }
                            else{
                                moviecost.setText(String.valueOf(Double.parseDouble(cost)));
                            }

                            Log.d("ticketcost", cost);
                        }
                    }
                    ticketselection = "2D";
                }
                else if(position == 2){
                    for(int i = 0; i < parseobjectslist.size(); i++){
                        if(parseobjectslist.get(i).getString("movieDate") == date_selected){
                            List<Object> arraylistregularcosts = new ArrayList<>();
                            arraylistregularcosts = (List<Object>) parseobjectslist.get(i)
                                    .get("threedCost");
                            cost = (String) arraylistregularcosts.get(time_selected_position);
                            if(checkboxpopcornandmovie.isChecked()){
                                moviecost.setText(String.valueOf(Double.parseDouble(cost)
                                        + 100));
                            }
                            else{
                                moviecost.setText(String.valueOf(Double.parseDouble(cost)));
                            }
                            Log.d("ticketcost", cost);
                        }
                    }
                    ticketselection = "3D";
                }
                else{
                    ticketselection = "unset";
                    moviecost.setText("0");
                }

                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void retryFunction(boolean isInternetPresent) {

        if(isInternetPresent) {

            noInternet.setVisibility(View.GONE);
            retry.setVisibility(View.GONE);
            wheel.setVisibility(View.VISIBLE);
            wheel.spin();
            mainLayout.setVisibility(View.VISIBLE);

            queryParseForMovieAndDetails();
        }
        else{
            retry.setVisibility(View.VISIBLE);
            wheel.stopSpinning();
            wheel.setVisibility(View.GONE);
            mainLayout.setVisibility(View.GONE);
            noInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // Dring startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely retrieve the event details
        ConnectionDetector connectionDetector = new ConnectionDetector(this);
        Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        if(isInternetPresent) {
           queryParseForMovieAndDetails();
        }
        else{
            retry.setVisibility(View.VISIBLE);
            wheel.stopSpinning();
            wheel.setVisibility(View.GONE);
            mainLayout.setVisibility(View.GONE);
            noInternet.setVisibility(View.VISIBLE);
        }

        choosempesaconfirmationcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser parseUser = ParseUser.getCurrentUser();

                if (parseUser != null) {

                    Intent intent = new Intent(getApplicationContext(), MpesaListActivity.class);
                    Log.d("movie_title", event_name.getText().toString());
                    intent.putExtra("reimburse", false);
                    intent.putExtra("movieName", event_name.getText().toString());
                    intent.putExtra("venue", venue);
                    intent.putExtra("movieDate", date_selected);
                    intent.putExtra("movieTime", time_selected);
                    intent.putExtra("ticketCost", moviecost.getText().toString());
                    intent.putExtra("ticketSelection", ticketselection);
                    if(checkboxpopcornandmovie.isChecked()){
                        intent.putExtra("popcornAndSoda", true);
                    }
                    else{
                        intent.putExtra("popcornAndSoda", false);
                    }
                    startActivity(intent);

                } else {
                    // show the signup or login screen
                    Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.putExtra("activity", "single_movie");
                    startActivity(intent);
                }
            }

        });

    }

    private void queryParseForMovieAndDetails() {

        final String movie = getArgsString();
        Log.d("movie", movie);

                    parseQuery1 = new ParseQuery<ParseObject>("movies");
                    parseQuery1.whereEqualTo("movieName", getArgsString());
                    parseQuery1.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObjectreturned, ParseException e) {
                            if (parseObjectreturned == null) {
                                Log.d("parsequery ", "parseobject null");
                            } else {
                                parseObject = parseObjectreturned;
                                updateDetailsView(parseObject);
                                parseQuery = new ParseQuery<>(movie.replaceAll(" ","_"));
                                parseQuery.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> list, ParseException e) {
                                        if(e == null){
                                            getSpinnerValues(list);
                                        }else{
                                            Toast.makeText(getApplicationContext(), "Movie cannot be loaded " +
                                                    e.getMessage() + ". Please try again later", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                            }
                        }
                    });

    }

    private void getSpinnerValues(List<ParseObject> list) {

        parseobjectslist = list;

        List<Object> arraylistdates = new ArrayList<>();
        for (int i = 0; i < parseobjectslist.size(); i++) {
            String date = parseobjectslist.get(i).getString("movieDate");
            arraylistdates.add(i, date);
        }

        String[] itemsdates = arraylistdates.toArray(new String[arraylistdates.size()]);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_row, itemsdates);
        dropdowndateselection.setAdapter(adapter);

        wheel.stopSpinning();
        wheel.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home) {

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private String getArgsString() {
        return argstringsingle;
    }

    private Bitmap BlurImage(Bitmap input) {

        Bitmap result = null;
        try {
            RenderScript rsScript = RenderScript.create(getApplicationContext());
            Allocation alloc = Allocation.createFromBitmap(rsScript, input);
            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rsScript, alloc.getElement());
            blur.setRadius(20);
            blur.setInput(alloc);
            result = Bitmap.createBitmap(input.getWidth(), input.getHeight(), input.getConfig());
            Allocation outAlloc = Allocation.createFromBitmap(rsScript, result);
            blur.forEach(outAlloc);
            outAlloc.copyTo(result);
            rsScript.destroy();
        }catch(RSIllegalArgumentException rsillegalargument){
            result = null;
            return result;
        }
        return result;
    }

    private void updateDetailsView(ParseObject parseObjectNew) {

        if(parseObjectNew.getBoolean("twodOnly")){
            twodOnly = true;
        }

        venue = parseObjectNew.getString("venue");

        ParseFile file = parseObjectNew.getParseFile("movieImage");

        View window = findViewById(R.id.layout_blur);

        byte[] bitmapdata = new byte[0];
        try {
            bitmapdata = file.getData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        Bitmap  testbitmap = BlurImage(bitmap);

        int sdk = android.os.Build.VERSION.SDK_INT;

        BitmapDrawable ob;

        Typeface typeFaces = TypeFaces.getTypeFace(getApplicationContext(),
                "fonts/Roboto-BoldItalic.ttf");

        event_name.setTypeface(typeFaces, Typeface.BOLD);

        if(testbitmap == null){
            ob = new BitmapDrawable(getResources(), bitmap);
        }
        else{
            ob = new BitmapDrawable(getResources(), testbitmap);
            ob.setAlpha(250);
            eventimage.setParseFile(file);
            eventimage.loadInBackground();
            event_name.setText(parseObjectNew.getString("movieName"));
        }

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            window.setBackgroundDrawable(ob);
        } else {
            window.setBackground(ob);
        }

        event_description.setText(parseObjectNew.getString("movieDescription"));

    }



}
