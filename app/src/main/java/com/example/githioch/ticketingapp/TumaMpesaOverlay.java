package com.example.githioch.ticketingapp;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.githioch.ticketingapp.util.MpesaService;

/**
 * Created by Githioch on 5/26/2015.
 */
public class TumaMpesaOverlay extends Service {

    Intent ServiceIntent;
    String a;
    IntentFilter filter;
    int i;
    private Button mAccount;
    private ImageButton mButton;
    private TextView mNameNumber;
    ViewGroup mTopView;
    String n;
    private Receiver objReceiver;
    public Vibrator vibrate;
    WindowManager wm;

    private class Receiver extends BroadcastReceiver
    {

        final TumaMpesaOverlay tumaMpesaOverlay;

        public void onReceive(Context context, Intent intent) {
            if (!currentTask().equals("com.android.stk") && android.os.Build.VERSION.SDK_INT <= 19
                    && mTopView != null)
            {
                wm.removeView(mTopView);
                stopSelf();


            }
        }

        private Receiver(TumaMpesaOverlay tumaMpesaOverlay)
        {
            this.tumaMpesaOverlay = tumaMpesaOverlay;

        }


    }

    public TumaMpesaOverlay()
    {
        n = "";
        a = "";
        i = 0;
    }
    public String currentTask()
    {
        return ((android.app.ActivityManager.RunningTaskInfo)((ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE))
                .getRunningTasks(1).get(0)).topActivity.getPackageName().trim();
    }

    @Override
    public void onCreate() {

        Log.d("tumampesa", "created");
        android.view.WindowManager.LayoutParams layoutparams = new android.view.WindowManager
                .LayoutParams(-1, -2, 2002, 40, -3);
        layoutparams.gravity = 48;
        layoutparams.x = 0;
        layoutparams.y = 0;
        wm = (WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        mTopView = (ViewGroup)((LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.floating_window, null);
        mTopView.setBackgroundColor(getResources().getColor(R.color.toast_background_color));
        wm.addView(mTopView, layoutparams);
        vibrate = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        mButton = (ImageButton)mTopView.findViewById(R.id.btn_cancel);
        mButton.setOnClickListener(new android.view.View.OnClickListener() {

            final TumaMpesaOverlay tumaMpesaOverlay = null;

            public void onClick(View view)
            {
                if (mTopView != null)
                {
                    wm.removeView(mTopView);
                    stopSelf();
                    Log.d("button" ,"service stopped");
                }
            }

        });
        mNameNumber = (TextView)mTopView.findViewById(R.id.text_testing);

        ServiceIntent = new Intent(this, MpesaService.class);
        filter = new IntentFilter("ping");
        objReceiver = new Receiver(null);
        registerReceiver(objReceiver, filter);
        startService(ServiceIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (objReceiver != null)
        {
            unregisterReceiver(objReceiver);
            if (startService(ServiceIntent) != null)
            {
                stopService(ServiceIntent);
                Log.d("tumapesa", "service stopped");
            }
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        n = "";
        a = "";
        i = 0;

        n = intent.getStringExtra("send_name");
        mNameNumber.setText(n);

        return START_STICKY;

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
