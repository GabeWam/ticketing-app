package com.example.githioch.ticketingapp;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * Created by Githioch on 4/2/2015.
 */
public class DisplayTicketFragment extends Fragment {

    private static String ARGSTRING = "";
    private ImageView imageView;

    public void setArgString(String qrcode) {
        ARGSTRING = qrcode;
    }

    private String getArgString(){
        return ARGSTRING;
    }


    // method that generates qr code
    private void generateQrCode() throws WriterException {


        com.google.zxing.Writer writer = new QRCodeWriter();
        String finaldata = Uri.encode(getArgString(), "utf-8");

        int QR_CODE_WIDTH = 500;
        int QR_CODE_HEIGHT = 500;
        BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE, QR_CODE_WIDTH, QR_CODE_HEIGHT);
        Bitmap ImageBitmap = Bitmap.createBitmap(QR_CODE_WIDTH, QR_CODE_HEIGHT, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < QR_CODE_WIDTH; i++) {//width
            for (int j = 0; j < QR_CODE_HEIGHT; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
            }
        }

        imageView.setImageBitmap(ImageBitmap);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_display_ticket, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.display_ticket);

        try {
            generateQrCode();
        } catch (WriterException e) {
            e.printStackTrace();
        }

        return rootView;

    }
}
