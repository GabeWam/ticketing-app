package com.example.githioch.ticketingapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.introduction.Introduction;
import com.example.githioch.ticketingapp.util.Util;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity implements MovieListFragment.OnMovieSelectedListener,
         MyTicketsFragment.OnTicketSelectedListener{

    private ArrayAdapter<String> drawerlist;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBar menu;
    private boolean manager;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent  = new Intent(this, Introduction.class);
        startActivity(intent);
        /*setContentView(R.layout.activity_main);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();
        menu.setDisplayShowHomeEnabled(true);
        menu.setLogo(R.drawable.ic_launcher);
        menu.setDisplayUseLogoEnabled(true);
        menu.setDisplayShowTitleEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main);

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        String[] values2;
        manager  = false;
        ParseUser parseUser = ParseUser.getCurrentUser();
            if (parseUser != null) {
                if(parseUser.getBoolean("Imax_Agent")) {
                    values2 = new String[]{"Browse", "Admin Page", "My Tickets","Reimbursement", "Logout"};
                    manager = true;
                }
                else{
                    values2 = new String[]{"Browse", "My Tickets","Reimbursement", "Logout"};
                }
            } else {
                values2 = new String[]{"Browse", "My Tickets","Reimbursement", "Login"};
            }
            ArrayList<String> list = new ArrayList<>();
            list.addAll(Arrays.asList(values2));
            drawerlist = new ArrayAdapter<>
                    (this, R.layout.layout_drawer, list);

            ListView listView = (ListView) findViewById(R.id.left_drawer);

            listView.setAdapter(drawerlist);

            listView.setOnItemClickListener(new DrawerItemClickListener());

        if(savedInstanceState == null){
            // calling fragment
            //The id passed into FragmentTransaction.add(), must be a child of the layout specified in setContentView().
            getSupportFragmentManager().beginTransaction().
                    add(R.id.frame_main, new MovieListFragment())
                    .commit();
        }*/

    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
            menu.setLogo(R.drawable.ic_launcher);
            menu.setDisplayUseLogoEnabled(true);
            menu.setDisplayShowTitleEnabled(true);
            menu.setTitle("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onMovieSelected(String movie) {

        Intent intent = new Intent(this,
                SingleMovieActivity.class);
        intent.putExtra("gig", movie);
        startActivity(intent);

        //slide from right to left
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {

        mDrawerLayout.closeDrawers();
        ParseUser.logOut();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Util util = new Util(getApplicationContext());
                util.deletePhoneNumberFromStore();
                util.deleteTicketsFromStore();
                util.deleteMoviesFromStore();

                Toast.makeText(getApplicationContext(),
                        "You have successfully logged out.", Toast.LENGTH_LONG).show();
                manager = false;
                String[] values3 = new String[]{"Browse", "My Tickets", "Login"};
                ArrayList<String> list2 = new ArrayList<>();
                list2.addAll(Arrays.asList(values3));
                updateData(values3);
            }
        }, 200);
    }

    private void updateData(String[] values3) {
        drawerlist.clear();
        if (values3 != null) {
            for (String string : values3) {
                drawerlist.insert(string, drawerlist.getCount());
            }
        }
        drawerlist.notifyDataSetChanged();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ParseQuery<ParseObject> parseQuery = new ParseQuery<>("movies");
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> list, ParseException e) {
                ParseObject.unpinAllInBackground(list, new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        Log.d("all movies stored", "deleted");
                    }
                });
            }
        });
    }

    @Override
    public void onTicketSelected(String gig) {

        Intent intent = new Intent(this,
                DisplayTicketActivity.class);
        intent.putExtra("qr", gig);
        startActivity(intent);

        //slide from right to left
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(parent,position);
        }

        private void selectItem(AdapterView<?> parent, int position) {

            if(manager) { //if IMAX guy
                if (position == 0) {
                    mDrawerLayout.closeDrawers();
                    commitEventListFragment();

                    menu.setLogo(R.drawable.ic_launcher);
                    menu.setDisplayUseLogoEnabled(true);
                    menu.setDisplayShowTitleEnabled(true);
                    menu.setTitle("");
                }

                if (position == 1) {
                    mDrawerLayout.closeDrawers();

                    menu.setDisplayUseLogoEnabled(false);
                    menu.setDisplayShowTitleEnabled(true);
                    menu.setTitle("Admin");

                    commitImaxFragment();
                }


                if (position == 2) {
                    menu.setDisplayUseLogoEnabled(false);
                    menu.setDisplayShowTitleEnabled(true);
                    menu.setTitle("My Tickets");

                    mDrawerLayout.closeDrawers();

                    android.os.Handler handler = new android.os.Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            commitMyTicketsFragment();

                        }
                    }, 200);

                }

                if(position == 3){
                    mDrawerLayout.closeDrawers();
                    Intent intent = new Intent(getApplicationContext(), MpesaListActivity.class);
                    intent.putExtra("reimburse", true);
                    startActivity(intent);
                }

                if (position == 4) {
                    if (parent.getItemAtPosition(position).toString().equals("Logout")) {
                        logoutUser();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                        startActivity(intent);
                    }
                    mDrawerLayout.closeDrawers();
                }
            }else{
                if (position == 0) {
                    mDrawerLayout.closeDrawers();
                    commitEventListFragment();

                    menu.setLogo(R.drawable.ic_launcher);
                    menu.setDisplayUseLogoEnabled(true);
                    menu.setDisplayShowTitleEnabled(true);
                    menu.setTitle("");
                }

                if (position == 1) {
                    menu.setDisplayUseLogoEnabled(false);
                    menu.setDisplayShowTitleEnabled(true);
                    menu.setTitle("My Tickets");

                    commitMyTicketsFragment();

                    mDrawerLayout.closeDrawers();

                }

                if(position == 2){
                    mDrawerLayout.closeDrawers();
                    Intent intent = new Intent(getApplicationContext(), MpesaListActivity.class);
                    intent.putExtra("reimburse", true);
                    startActivity(intent);
                }

                if (position == 3) {
                    if (parent.getItemAtPosition(position).toString().equals("Logout")) {
                        logoutUser();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                        startActivity(intent);
                    }
                    mDrawerLayout.closeDrawers();
                }
            }

        }
    }

    private void commitMyTicketsFragment() {
        MyTicketsFragment displayTicketFragment = new MyTicketsFragment();
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_main, displayTicketFragment);
        // transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    private void commitImaxFragment() {
        ImaxUserFragment imaxUserFragment = new ImaxUserFragment();
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.frame_main, imaxUserFragment);
        //transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    private void commitEventListFragment() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.frame_main, new MovieListFragment())
                .commit();
    }
}
