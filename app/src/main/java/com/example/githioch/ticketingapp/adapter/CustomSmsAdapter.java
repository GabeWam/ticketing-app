package com.example.githioch.ticketingapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.model.Mpesa;

import java.util.List;

/**
 * Created by Githioch on 3/24/2015.
 */
public class CustomSmsAdapter extends BaseAdapter {

    private final Activity activity;
    private LayoutInflater inflater;
    private final List<Mpesa> mpesaList;

    public CustomSmsAdapter(Activity activity, List<Mpesa> mpesaSms) {
        this.activity = activity;
        this.mpesaList = mpesaSms;

    }

    @Override
    public int getCount() {
        return mpesaList.size();
    }

    @Override
    public Object getItem(int position) {
        return mpesaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (convertView == null)
            convertView = inflater.inflate(R.layout.sms_row, null);

        TextView dateview = (TextView) convertView.findViewById(R.id.time_and_date);

        TextView textView = (TextView) convertView.findViewById(R.id.sms);


        Mpesa mpesa = mpesaList.get(position);
        textView.setText(mpesa.getSmsbody());
        dateview.setText(mpesa.getSmsDate());
        //Log.d("textview message: ", mpesa.getSmsbody());

        return convertView;
    }
}
