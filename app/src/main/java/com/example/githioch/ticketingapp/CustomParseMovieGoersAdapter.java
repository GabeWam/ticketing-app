package com.example.githioch.ticketingapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.githioch.ticketingapp.model.Movie;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.util.List;

/**
 * Created by Githioch on 3/30/2015.
 */
public class CustomParseMovieGoersAdapter extends ParseQueryAdapter<ParseObject> {


    public CustomParseMovieGoersAdapter(Context context) {
        // Use the QueryFactory to construct a PQA that will only show
        // Todos marked as high-pri
        super(context, new ParseQueryAdapter.QueryFactory<ParseObject>() {
            public ParseQuery create() {
                ParseQuery<ParseObject> parsequery = ParseQuery.getQuery("hello");
                parsequery.fromLocalDatastore();
                parsequery.orderByAscending("phoneNumber");
                return parsequery;
            }
        });
    }


    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(final ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.list_row_movies_goers, null);
        }
        super.getItemView(object, v, parent);

        ImageView checkedInImage = (ImageView) v.findViewById(R.id.image_checked_in);

        TextView objectId = (TextView) v.findViewById(R.id.objectId);
        objectId.setText(object.getString("ticketQrCode"));

        TextView phoneNumber = (TextView) v.findViewById(R.id.phone_number_movie_goer);
        phoneNumber.setText(object.getString("phoneNumber"));

        TextView checkedinornotview = (TextView) v.findViewById(R.id.checked_in_or_not_view);
        boolean checkIn = object.getBoolean("checkedIn");
        if(checkIn){
            checkedinornotview.setText("Checked In");
            checkedInImage.setImageResource(R.drawable.greentick);
        }
        else{
            checkedinornotview.setText("NOT Checked In");
            checkedInImage.setImageResource(R.drawable.redcross);
        }

        return v;
    }
}
