package com.example.githioch.ticketingapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.model.Movie;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import java.util.List;

/**
 * Created by Githioch on 3/30/2015.
 */
public class CustomParseTicketsAdapter extends ParseQueryAdapter<ParseObject> {

    private List<Movie> movieItems;

    public CustomParseTicketsAdapter(Context context, ParseQueryAdapter.QueryFactory<ParseObject> queryFactory) {
        // Use the QueryFactory to construct a PQA that will only show
        // Todos marked as high-pri
        super(context, queryFactory);
    }

    // Customize the layout by overriding getItemView
    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        if (v == null) {
            //v = View.inflate(getContext(), R.layout.list_row, null);
            v = View.inflate(getContext(), R.layout.list_row_tickets_liked, null);
        }
        super.getItemView(object, v, parent);
        // Add and download the image
        ParseImageView todoImage = (ParseImageView) v.findViewById(R.id.thumbnail_ticket);
        ParseFile imageFile = object.getParseFile("image");
        if (imageFile != null) {
            todoImage.setParseFile(imageFile);
            todoImage.loadInBackground();
        }
// Add the title view
        //TextView title = (TextView) v.findViewById(R.id.title);
        //title.setText(object.getString("Event_Name"));
        TextView title = (TextView) v.findViewById(R.id.title_ticket);
        title.setText(object.getString("title"));


        //TextView venue = (TextView) v.findViewById(R.id.venue);
        //venue.setText(object.getString("Venue"));
        TextView venue = (TextView) v.findViewById(R.id.venue_ticket);
        venue.setText(object.getString("venue"));

      //  TextView eventdate = (TextView) v.findViewById(R.id.eventdate);
        //eventdate.setText(object.getString("Event_Date"));
        TextView eventdate = (TextView) v.findViewById(R.id.eventdate_ticket);
        eventdate.setText(object.getString("date"));


        return v;
    }
}
