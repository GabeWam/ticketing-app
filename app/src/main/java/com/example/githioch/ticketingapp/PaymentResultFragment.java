package com.example.githioch.ticketingapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.Util;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Githioch on 4/1/2015.
 */

//TODO saving tickets issue
public class PaymentResultFragment extends Fragment {
    private static String ARGSTRING = "";
    private static String event_name = "";
    private static Bitmap ImageBitmap;
    // Log tag
    private final String TAG_PAYMENT_SUCCESSFUL_FRAGMENT = PaymentResultFragment.class.getSimpleName();
    private final int QR_CODE_WIDTH = 500;
    private final int QR_CODE_HEIGHT = 500;
    ImageView imageView;
    TextView gototickts;
    TextView showticket;
    android.support.v7.app.ActionBar menu;
    private String result;
    private String ID;
    private String phoneNumber;
    private String ticketSelection;
    private String confirmationCode;
    private ProgressWheel wheel;
    private TextView no_internet;
    private LinearLayout layout;
    private Button retry;
    private TextView textView;
    private boolean saveticket;
    private String movieTime;
    private String movieDate;
    private String ticketCost;
    private String venue;
    private boolean popcornAndSoda;
    private Button gotomainpage;

    public void setEventName(String eventName) {
        event_name = eventName;
    }

    private String getEvent_name() {
        return event_name;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        setEventName(getArguments().getString("movieName"));
        setTicketSelection(getArguments().getString("ticketSelection"));
        setConfirmationCode(getArguments().getString("confirmationCode"));
        setMovieTime(getArguments().getString("movieTime"));
        setMovieDate(getArguments().getString("movieDate"));
        setTicketCost(getArguments().getString("ticketCost"));
        setPopcornAndSoda(getArguments().getBoolean("popcornAndSoda"));
        setVenue(getArguments().getString("venue"));

        View rootView = inflater.inflate(R.layout.fragment_payment_successful, container, false);
        textView = (TextView) rootView.findViewById(R.id.payment_result_textview);
        gototickts = (TextView) rootView.findViewById(R.id.gototickets);
        imageView = (ImageView) rootView.findViewById(R.id.image_qr_successful_payment);
        gotomainpage = (Button) rootView.findViewById(R.id.go_to_main_page);
        wheel = (ProgressWheel) rootView.findViewById(R.id.progress_wheel);
        no_internet = (TextView) rootView.findViewById(R.id.no_internet);
        layout = (LinearLayout) rootView.findViewById(R.id.layout);
        retry = (Button) rootView.findViewById(R.id.retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
                boolean isInternetPresent = connectionDetector.isConnectingToInternet();
                retryFunction(isInternetPresent);
            }
        });

        Log.d("name", getEvent_name());
        Log.d("ticket", getTicketSelection());

        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity());
        Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        if(isInternetPresent) {
            performPayment();
        }
        else{
            wheel.stopSpinning();
            wheel.setVisibility(View.GONE);
            layout.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
            retry.setVisibility(View.VISIBLE);
        }


        gotomainpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        return rootView;
    }

    private void performPayment() {

        Util util = new Util(getActivity().getApplicationContext());
        setPhoneNumber(util.findPhoneNumber());

        final HashMap<String, Object> params = new HashMap<String, Object>();
        String get = getPhoneNumber();
        Log.d("get", get);
        params.put("phone_number", get);
        params.put("confirmation_code", this.confirmationCode);
        params.put("movie_name", getEvent_name());
        params.put("movieDate", movieDate);
        params.put("movieTime", movieTime);
        params.put("ticketCost", Double.parseDouble(ticketCost));
        params.put("ticket_selection", getTicketSelection());
        params.put("popcornAndSoda", getPopcornAndSoda());
        ParseCloud.callFunctionInBackground("process_ticket", params, new FunctionCallback<ArrayList<String>>() {
            @Override
            public void done(ArrayList<String> strings, ParseException e) {
                if (e == null) {

                    if(!strings.get(0).equals("no_ticket")){
                        ((ConfirmPaymentActivity) getActivity()).setPaymentState(true);

                        setID(String.valueOf(strings.get(0)));
                        Log.d("ticket", String.valueOf(strings.get(0)));
                        Log.d("reimburse", String.valueOf(strings.get(1)));

                        ParseObject parseObject =  new ParseObject("tickets");
                        parseObject.put("phoneNumber", getPhoneNumber());
                        parseObject.put("ticketQrCode", getID());
                        parseObject.put("movieName", getEvent_name());
                        parseObject.put("movieDate", movieDate);
                        parseObject.put("movieTime", movieTime);
                        parseObject.put("selection", getTicketSelection());
                        parseObject.put("popcornAndSoda", getPopcornAndSoda());
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e == null){
                                    saveToMyTickets();
                                    try {
                                        generateQrCode();
                                        textView.setText("Payment Successful!");
                                        wheel.stopSpinning();
                                        wheel.setVisibility(View.GONE);
                                    } catch (WriterException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                    }else{
                        Log.d("ticket", String.valueOf(strings.get(0)));
                        textView.setText("Payment not found. Are you sure you have sent the correct payment?");
                        wheel.stopSpinning();
                        wheel.setVisibility(View.GONE);
                        gototickts.setVisibility(View.GONE);
                        gotomainpage.setVisibility(View.GONE);
                    }

                } else {
                    Log.d("error", e.toString());
                }
            }

        });
    }

    private void retryFunction(boolean isInternetPresent) {

        if(isInternetPresent){

            retry.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);
            wheel.setVisibility(View.VISIBLE);
            wheel.spin();
            no_internet.setVisibility(View.GONE);

            performPayment();
        }
        else{
            wheel.setVisibility(View.GONE);
            retry.setVisibility(View.VISIBLE);
            no_internet.setVisibility(View.VISIBLE);
            layout.setVisibility(View.GONE);
        }
    }

    /**
     * saves ticket to "My Tickets" after a successful payment
     * gets event details from parse and saves it locally together with
     * the qr code
     *
     */
    private void saveToMyTickets() {

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("tickets");
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {

                Log.d("payment list", String.valueOf(list.size()));

                if(e == null){
                    ParseObject parseObject =  new ParseObject("tickets");
                    parseObject.put("phoneNumber", getPhoneNumber());
                    parseObject.put("ticketQrCode", getID());
                    parseObject.put("movieName", getEvent_name());
                    parseObject.put("movieDate", movieDate);
                    parseObject.put("movieTime", movieTime);
                    parseObject.put("selection", getTicketSelection());
                    parseObject.put("popcornAndSoda", getPopcornAndSoda());
                    parseObject.put("venue", getVenue());

                    list.add(parseObject);
                    try {
                        ParseObject.pinAll(list);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });


    }

    // method that generates qr code
    private void generateQrCode() throws WriterException {

        String data = getID();

        com.google.zxing.Writer writer = new QRCodeWriter();
        String finaldata = Uri.encode(data, "utf-8");

        BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE, QR_CODE_WIDTH, QR_CODE_HEIGHT);
        ImageBitmap = Bitmap.createBitmap(QR_CODE_WIDTH, QR_CODE_HEIGHT, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < QR_CODE_WIDTH; i++) {//width
            for (int j = 0; j < QR_CODE_HEIGHT; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
            }
        }

        if (ImageBitmap != null) {

            imageView.setImageBitmap(ImageBitmap);
        } else {
            Toast.makeText(getActivity(), "Error making qrcode",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;

    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTicketSelection() {
        return this.ticketSelection;
    }

    public void setTicketSelection(String ticketSelection) {
        this.ticketSelection = ticketSelection;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setMovieTime(String movieTime) {
        this.movieTime = movieTime;
    }

    public void setMovieDate(String movieDate) {
        this.movieDate = movieDate;
    }

    public void setTicketCost(String ticketCost) {
        this.ticketCost = ticketCost;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public boolean getPopcornAndSoda() {
        return popcornAndSoda;
    }

    public void setPopcornAndSoda(boolean popcornAndSoda) {
        this.popcornAndSoda = popcornAndSoda;
    }
}
