package com.example.githioch.ticketingapp;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class ConfirmationCodeFragment extends DialogFragment {
    public interface ConfirmationCodeListener {
        public void onDialogPositiveClick(DialogFragment dialog);

    }

    public interface ConfirmationCodeCancel {
        public void onDialogNegativeClick(DialogFragment dialog);

    }



    private String mTitle;
    private String mMessage;
    private ConfirmationCodeListener mListener;
    private ConfirmationCodeCancel mListenercancel;


    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }


    public static ConfirmationCodeFragment newInstance(String title, String message, ConfirmationCodeListener listener,
                                                       ConfirmationCodeCancel codeCancel) {
        ConfirmationCodeFragment fragment = new ConfirmationCodeFragment();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.mListenercancel = codeCancel;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(mMessage)
                .setTitle(mTitle);


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mListener != null) {
                    mListener.onDialogPositiveClick(ConfirmationCodeFragment.this);
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListenercancel != null) {
                    mListenercancel.onDialogNegativeClick(ConfirmationCodeFragment.this);
                }
            }
        });


        return builder.create();
    }
}
