package com.example.githioch.ticketingapp.app;

import android.app.Application;

import com.example.githioch.ticketingapp.R;
import com.example.githioch.ticketingapp.model.Movie;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Created by Githioch on 5/18/2015.
 */
//TODO image for failure to load
public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        ParseObject.registerSubclass(Movie.class);

        Parse.initialize(this, "TCOVvO1oyohRcxxJSJVzM7P86eUXq5CTUlYGvrOE",
                "GCXKhFpHGWlUEqqKqwJtYDCC1BoV1KRbnZ8VCIT4");

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_launcher).showImageOnFail(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory().cacheOnDisc().build();
        // Create global configuration and initialize ImageLoader with this
        // configuration
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder
                (getApplicationContext()).defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);



    }
}
