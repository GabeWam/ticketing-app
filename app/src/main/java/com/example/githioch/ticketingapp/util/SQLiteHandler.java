package com.example.githioch.ticketingapp.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

/**
 * Created by Githioch on 3/14/2015.
 */
public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_MY_TICKETS = "mytickets";

    // Login Table Columns names
    // private static final String KEY_ID = "id";
    private static final String KEY_EVENT_NAME = "Event_Name";
    private static final String KEY_EVENT_VENUE = "Venue";
    private static final String KEY_EVENT_DATE = "Event_Date";
    private static final String KEY_EVENT_DESCRIPTION = "Event_Description";
    private static final String KEY_EVENT_IMAGE = "imagetest";
    private static final String KEY_EVENT_QRCODE_DATA = "qrcodedata";


    private SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);*/

        // TODO primary key

        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_MY_TICKETS + "("
                + KEY_EVENT_NAME + " TEXT," + KEY_EVENT_VENUE + " TEXT," + KEY_EVENT_DATE +
                " TEXT," + KEY_EVENT_DESCRIPTION + " TEXT," + KEY_EVENT_QRCODE_DATA +
                " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MY_TICKETS);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     */
    public void addEvent(String eventname, String eventvenue, String eventdate, String eventdescription
            , String qrcodedata) {
        SQLiteDatabase db = this.getWritableDatabase();

        //  byte [] array = convertToByteArray(eventimage);


        // Log.d("saved bytes", String.valueOf(array.length));

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_NAME, eventname); // Event Name
        values.put(KEY_EVENT_VENUE, eventvenue); // Event Venue
        values.put(KEY_EVENT_DATE, eventdate); // event date
        values.put(KEY_EVENT_DESCRIPTION, eventdescription);
        // values.put(KEY_EVENT_IMAGE, array);
        values.put(KEY_EVENT_QRCODE_DATA, qrcodedata);

        // Inserting Row
        long id = db.insert(TABLE_MY_TICKETS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    private byte[] convertToByteArray(Bitmap eventimage) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        eventimage.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();

    }

    /**
     * Getting event data from database
     */
    public HashMap<String, String> getEventDetailsExceptImage() {
        HashMap<String, String> event = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_MY_TICKETS;


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            event.put(KEY_EVENT_NAME, cursor.getString(0));

            event.put(KEY_EVENT_VENUE, cursor.getString(1));
            event.put(KEY_EVENT_DATE, cursor.getString(2));
            event.put(KEY_EVENT_DESCRIPTION, cursor.getString(3));
            event.put(KEY_EVENT_QRCODE_DATA, cursor.getString(4));

        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching event details except image from Sqlite:");

        return event;
    }

    public HashMap<String, byte[]> getEventImage() {
        HashMap<String, byte[]> eventimage = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_MY_TICKETS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            eventimage.put(KEY_EVENT_IMAGE, cursor.getBlob(4));

        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching image from Sqlite");

        return eventimage;
    }

    /**
     * Getting user login status return true if rows are there in table
     */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MY_TICKETS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();

        // return row count
        return rowCount;
    }

    /**
     * Recreate database Delete all tables and create them again
     */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_MY_TICKETS, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}
