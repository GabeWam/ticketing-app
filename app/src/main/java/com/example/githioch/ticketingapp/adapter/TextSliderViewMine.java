package com.example.githioch.ticketingapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.example.githioch.ticketingapp.R;

/**
 * Created by Githioch on 6/29/2015.
 */
public class TextSliderViewMine extends BaseSliderView {
    public TextSliderViewMine(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.layout_introduction,null);
        ImageView target = (ImageView)v.findViewById(R.id.slider_image);
        TextView description = (TextView)v.findViewById(R.id.say_what);
        description.setText(getDescription());
        bindEventAndShow(v, target);
        return v;
    }
}
