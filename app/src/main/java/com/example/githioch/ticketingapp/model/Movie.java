package com.example.githioch.ticketingapp.model;

import android.graphics.Bitmap;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

/**
 * Created by Githioch on 3/7/2015.
 * This model class will be used to provide movie objects data to list view after parsing the json.
 */
@ParseClassName("Movie")
public class Movie extends ParseObject {


    private String movieName;
    private String movieDescription;
    private ParseFile movieImage;
    private String venue;
    private String eventdescription;

    public Movie() {
    }

    public Movie(String movieName, String venue, String movieDescription, ParseFile movieImage){
        this.movieName = movieName;
        this.venue = venue;
        this.movieDescription = movieDescription;
        this.movieImage = movieImage;
    }


    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public ParseFile getMovieImage() {
        return movieImage;
    }

    public void setMovieImage(ParseFile movieImage) {
        this.movieImage = movieImage;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getEventdescription() {
        return eventdescription;
    }

    public void setEventdescription(String eventdescription) {
        this.eventdescription = eventdescription;
    }


}
