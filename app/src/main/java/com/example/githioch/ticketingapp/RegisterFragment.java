package com.example.githioch.ticketingapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.SQLiteHandler;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.HashMap;

/**
 * Created by Githioch on 1/20/2015.
 * For the user to register an account
 */
public class RegisterFragment extends Fragment {

    private static String TAG = RegisterFragment.class.getSimpleName();

    String jsonresult = null;
    private TextView displayview = null;
    private String username;
    private String emailaddress;
    private String password1;
    private String password2;
    private EditText emailaddressview;

    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private String activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getActivity().getIntent();
        activity = intent.getStringExtra("activity");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_register_fragment, container, false);

        final EditText phonenumberview = (EditText) rootView.findViewById(R.id.phonenumber);

        final EditText email = (EditText) rootView.findViewById(R.id.email);

        final EditText password1view = (EditText) rootView.findViewById(R.id.password1);

        final EditText password2view = (EditText) rootView.findViewById(R.id.password2);

        displayview = (TextView) rootView.findViewById(R.id.displayresults);

        Button register = (Button) rootView.findViewById(R.id.register);

        Button linklogin = (Button) rootView.findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectionDetector connectionDetector = new ConnectionDetector(getActivity()
                        .getApplicationContext());
                Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

                if (isInternetPresent){

                    String numnberasstring = phonenumberview.getText().toString().trim();

                    boolean validphonenumnber = validatePhoneNumber(numnberasstring);

                    if(validphonenumnber){

                        String emailstring = email.getText().toString().trim();
                        boolean validateEmialAddress = validateEmailAddress(emailstring);

                        if(validateEmialAddress){

                            password1 = password1view.getText().toString().trim();
                            password2 = password2view.getText().toString().trim();

                            boolean validPassword = validatePassword(password1, password2);

                            if(validPassword){
                                registerUser(numnberasstring,emailstring, password1);

                            }

                        }
                    }
                }
                else{
                    hideDialog();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Please connect to the Internet and try again.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });


        // Link to Login Screen
        linklogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(),
                        SignInActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });

        return rootView;
    }


    private boolean validatePhoneNumber(String phonenumber) {

        if (phonenumber.matches("[0-9]+") && phonenumber.length() == 10) {
            return true;
        } else {
            // TODO more error checking for phone number
            Toast.makeText(getActivity().getApplicationContext(), "Enter valid phone number."
                    , Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    private boolean validatePassword(String password1, String password2) {

        if (password1.isEmpty() || password2.isEmpty()) {
            Toast.makeText(getActivity().getApplicationContext(),
                    "Please fill in all password fields."
                    , Toast.LENGTH_SHORT).show();
            return false;
        } else if (!password1.equals(password2)) {
            Toast.makeText(getActivity().getApplicationContext(), "Passwords do not match."
                    , Toast.LENGTH_SHORT).show();
            return false;

        } else {
            return true;
        }
    }

    //TODO test .co.ke email address pattern server interaction
    private boolean validateEmailAddress(String s) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPatternLikeDotCoDotKE = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
        // get values from repesctive fields
        if (!s.equals("")) {
            emailaddress = s;

            if (emailaddress.matches(emailPattern) || emailaddress.matches(emailPatternLikeDotCoDotKE)) {
                return true;

            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Invalid email address."
                        , Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {

            Toast.makeText(getActivity().getApplicationContext(), "Please fill in your email address."
                    , Toast.LENGTH_SHORT).show();
            return false;
        }


    }

    private void registerUser(final String phonenumber_register, String emailstring, final String password_register) {


        pDialog.setMessage("Registering ...");
        showDialog();

        //save phone number for use when buying tickets

        ParseUser user = new ParseUser();
        // setting username as phone number for now;
        // I don't want users to have to enter too much info to register
        user.setUsername(phonenumber_register);
        user.setEmail(emailstring);
        user.put("phoneNumber", phonenumber_register);
        user.setPassword(password_register);

        // other fields can be set just like with ParseObject
        // user.put("phone", "650-253-0000");

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    // Hooray! Let them use the app now.
                    hideDialog();

                   // sendEmail(emailaddress_register);
                    Toast.makeText(getActivity().getApplicationContext(),
                            "You have been successfully registered",
                            Toast.LENGTH_LONG).show();

                    if(activity == null) {
                        Intent intent = new Intent(
                                getActivity(),
                                MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                    else{
                        getActivity().finish();
                    }

                } else {
                    //TODO test toast
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    if (getActivity() != null) {
                        Toast.makeText(getActivity().getApplicationContext(), "Registering not successful" +
                                ". Try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }

    private void sendEmail(String emailaddress_register) {


        HashMap<String, Object> params = new HashMap<>();
        // TODO
        params.put("to", "gwamunyu11@alaalumni.org");

        // params.put("to",emailaddress_register);

        ParseCloud.callFunctionInBackground("mySendGridFunction", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
                if (e == null) {
                    // result is "Hello world!"
                    Log.d("sendgrid", result);
                }
            }
        });
    }

    private void hideDialog() {

        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();


    }


}




