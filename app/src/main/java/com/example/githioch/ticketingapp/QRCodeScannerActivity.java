/*
 * Basic no frills app which integrates the ZBar barcode scanner with
 * the camera.
 * 
 * Created by lisah0 on 2012-02-24
 */
package com.example.githioch.ticketingapp;


import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.util.List;

import me.dm7.barcodescanner.core.ViewFinderView;

/* Import ZBar Class files */

//TODO testing
//TODO show admin that user is already checked in checkin field is already true

public class QRCodeScannerActivity extends FragmentActivity
        implements MessageDialogFragment.MessageDialogListener {

    static {
        System.loadLibrary("iconv");
    }

    TextView scanText;
    Button scanButton;
    ImageScanner scanner;
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private String date;
    private String time;
    private String movie_name;
    PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                String results = "";
                for (Symbol sym : syms) {
                    results += sym.getData();
                    barcodeScanned = true;
                }

                // if not then the ticket is not valid
                validateTicket(results);

            }
        }
    };
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };
    // Mimic continuous auto-focusing
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        }
        return c;
    }

    //TODO skipping frames with camera preview I think
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.qr_code_scanner);

        date = getIntent().getStringExtra("date");


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);


        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);

        ViewFinderView viewFinderView = new ViewFinderView(this);


        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        preview.setBackgroundColor(-16777216);

        preview.addView(mPreview);
        preview.addView(viewFinderView);

        RelativeLayout relativeLayoutControls = (RelativeLayout) findViewById(R.id.controls_layout);
        relativeLayoutControls.bringToFront();

        scanText = (TextView) findViewById(R.id.qr_results_view);

    }

    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    //TODO add reason for invalid ticket

    private void validateTicket(String results) {

        ParseQuery<ParseObject> parsequery = ParseQuery.getQuery("hello");
        parsequery.fromLocalDatastore();
        Log.d("dateqr", date);
        parsequery.whereEqualTo("movieDate", date);
        ParseObject parseObject = null;
        try {
            List<ParseObject> parseObjectList =  parsequery.find();
            boolean found = false;

            for (int i = 0; i < parseObjectList.size(); i++) {
                Log.d("size", String.valueOf(parseObjectList.size()));
                if(parseObjectList.get(i).getString("ticketQrCode").equals(results)){
                    found = true;
                    parseObject = parseObjectList.get(i);
                    if(!parseObject.getBoolean("checkedIn")){
                        parseObject.put("checkedIn", true);
                        try {
                            parseObject.pin();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        //TODO call save instead of querying again
                        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("tickets");
                        Log.d("qr code", parseObject.getString("ticketQrCode"));
                        parseQuery.whereEqualTo("ticketQrCode", parseObject.getString("ticketQrCode"));
                        parseQuery.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                list.get(0).put("checkedIn", true);
                                list.get(0).saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Log.d("checked in", "saved");
                                    }
                                });
                            }
                        });

                    /*Integer amount = (Integer) parseObject.getNumber("amountPaid");

                    ParseObject parseObject1 = new ParseObject("numberticketsvalid"+movie_name);
                    parseObject1.put("amount", amount);
                    parseObject1.pinInBackground();*/

                        String eventname = parseObject.getString("movieName");
                        boolean popcornandsoda = parseObject.getBoolean("popcornAndSoda");
                        showMessageDialog("Valid", eventname, popcornandsoda);

                        break;
                    }else{
                        String eventname = parseObject.getString("movieName");
                        boolean popcornandsoda = parseObject.getBoolean("popcornAndSoda");
                        showMessageDialog("User already checked in", eventname, popcornandsoda);

                        break;
                    }
                }
            }
            if(!found){
                showMessageDialog("NOT valid - Ticket not found", null, false);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void showMessageDialog(String validity, String eventname, boolean popcornandsoda) {

        DialogFragment fragment = null;
        if(eventname == null){
            fragment = MessageDialogFragment.newInstance("Scan Results", validity, this);
        }
        else if (popcornandsoda){
            fragment = MessageDialogFragment.newInstance("Scan Results",
                    validity+" - "+eventname +"\nAdd Popcorn and Soda", this);
        }
        else{
            fragment = MessageDialogFragment.newInstance("Scan Results",
                    validity+" - "+eventname, this);

        }

        fragment.show(getSupportFragmentManager(), "scan_results");
        scanText.setText("");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        if (barcodeScanned) {
            barcodeScanned = false;
            scanText.setText("Scanning...");
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            previewing = true;
            mCamera.autoFocus(autoFocusCB);
        }
    }

}
