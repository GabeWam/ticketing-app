package com.example.githioch.ticketingapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.example.githioch.ticketingapp.util.SQLiteHandler;

/**
 * Created by Githioch on 2/16/2015.
 * Updated 2/26/15
 * Code for user to log in
 */


public class SignInActivity extends FragmentActivity {

    private final String TAG = SignInActivity.class.getSimpleName();
    String password_signin = null;
    String jsonresult_signin;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private  String phone_number_signin = null;


    // String password2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);


        // calling fragment
        if (savedInstanceState == null) {

            SignInFragment signInFragment
                    = new SignInFragment();


            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.frame_payment, signInFragment);
            // transaction.addToBackStack(null);

            transaction.commit();

        }

    }




}
