package com.example.githioch.ticketingapp.util;

import android.content.Context;
import android.util.Log;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Githioch on 3/16/2015.
 * Stores useful functions that can be used in many instances in the overall code
 */
public class Util {

    private final Context _context;

    public Util(Context context) {
        this._context = context;
    }

    public String findPhoneNumber(){

        return ParseUser.getCurrentUser().getUsername();

    }



    public void deleteTicketsFromStore() {

        // FOR DELETING TICKETS
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("tickets");
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("found", "tickets");
                for (int i = 0; i < list.size(); i++) {

                    list.get(i).unpinInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("tickets", "deleted");
                        }
                    });
                }

            }
        });
    }


    public void deleteLikedEventsFromStore() {

        // FOR DELETING LIKED EVENTS
        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("eventlike");
        parseQuery.fromLocalDatastore();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                Log.d("found", "events");
                for (int i = 0; i < list.size(); i++) {

                    list.get(i).unpinInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("object", "deleted");
                        }
                    });
                }

            }
        });

    }

    public void deletePhoneNumberFromStore() {
        ParseQuery<ParseObject> parseQuery2 = ParseQuery.getQuery("phone_number");
        parseQuery2.fromLocalDatastore();
        parseQuery2.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {

                for (int i = 0; i < list.size(); i++) {

                    list.get(i).unpinInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("phone number", "deleted");
                        }
                    });
                }

            }
        });
    }

    public void deleteMoviesFromStore() {
        ParseQuery<ParseObject> parseQuery2 = ParseQuery.getQuery("movies");
        parseQuery2.fromLocalDatastore();
        parseQuery2.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {

                for (int i = 0; i < list.size(); i++) {

                    list.get(i).unpinInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("movie", "deleted");
                        }
                    });
                }

            }
        });
    }
}
