package com.example.githioch.ticketingapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.githioch.ticketingapp.util.ConnectionDetector;
import com.example.githioch.ticketingapp.util.SQLiteHandler;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RefreshCallback;

import java.util.List;

/**
 * Created by Githioch on 2/16/2015.
 * Updated 2/26/15
 * Code for user to log in
 */

//TODO make invaloid login faster
public class SignInFragment extends Fragment {

    private final String TAG = SignInFragment.class.getSimpleName();
    String password_signin = null;
    String jsonresult_signin;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private  String phone_number_signin = null;
    private String activity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getActivity().getIntent();
        activity = intent.getStringExtra("activity");


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_signin_fragment, container, false);

        final EditText signin_phone_number = (EditText) rootView.findViewById(R.id.phone_number_signin);
        final EditText signin_password = (EditText) rootView.findViewById(R.id.password_signin);
        final Button signin_button = (Button) rootView.findViewById(R.id.signin_button);
        final Button signin_register = (Button) rootView.findViewById(R.id.signin_register);
        Button resetpassword = (Button) rootView.findViewById(R.id.reset_password);

        resetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordFragment forgotPasswordFragment
                        = new ForgotPasswordFragment();


                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.frame_payment, forgotPasswordFragment);
                transaction.addToBackStack(null);

                transaction.commit();
            }
        });

        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(true);

        signin_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegisterFragment registerFragment
                        = new RegisterFragment();


                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.frame_payment, registerFragment);
                // transaction.addToBackStack(null);

                transaction.commit();
            }
        });


        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phone_number_signin = signin_phone_number.getText().toString();
                password_signin = signin_password.getText().toString();

                // Check for empty data in the form
                if (phone_number_signin.trim().length() > 0 && password_signin.trim().length() > 0) {

                    // login user
                    checkLogin(phone_number_signin, password_signin);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }

            }
        });

        return  rootView;
    }



    private void checkLogin(final String phonenumber, final String password) {

        pDialog.setMessage("Logging in ...");
        showDialog();


        // TODO check if database has values

        ConnectionDetector connectionDetector = new ConnectionDetector(getActivity().getApplicationContext());
        Boolean isInternetPresent = connectionDetector.isConnectingToInternet();

        if (isInternetPresent) {

            ParseUser.logInInBackground(phonenumber, password, new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        // Hooray! The user is logged in.
                        // user successfully logged in
                        // Create login session
                        // session.setLogin(true);

                        // save login details in new database so that user won't have to register
                        // Parse automatically saves the session to disk


                        //update current user with server data
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        currentUser.fetchInBackground(new GetCallback<ParseObject>() {
                            public void done(ParseObject object, ParseException e) {
                                if (e == null) {
                                    ParseUser currUser = (ParseUser) object;
                                    // Do Stuff with currUSer
                                } else {
                                    // Failure!
                                }
                            }
                        });

                        hideDialog();

                        if(activity == null) {
                            Intent intent = new Intent(
                                    getActivity(),
                                    MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                        else{
                            getActivity().finish();
                        }

                    } else {
                        // Signup failed. Look at the ParseException to see what happened.
                        hideDialog();
                        if (getActivity() != null) {
                            Toast.makeText(getActivity().getApplicationContext(), e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });


        } else { // not connected to the Internet
            pDialog.dismiss();
            Toast.makeText(getActivity().getApplicationContext(),
                    "Please connect to the Internet and try again", Toast.LENGTH_LONG).show();

        }
        // }
    }


    private void showDialog() {

        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {

        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }

    }

}
